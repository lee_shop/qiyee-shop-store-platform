package com.qiyee.shop.store.statistics.api;

import com.qiyee.shop.entity.customer.statistics.StoreInfoAreaStatistics;
import com.qiyee.shop.store.statistics.service.StoreInfoAreaStatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/store/statistics")
@RestController
@Api("店铺地区统计服务接口")
public class StoreInfoAreaStatisticsApi {
    @Autowired
    private StoreInfoAreaStatisticsService areaStatisticsService;

    /**
     * 统计店铺地区数量（按省级地区分组）
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 返回店铺地区数量统计
     */
    @ApiOperation(value = "统计店铺地区数量（按省级地区分组）", notes = "统计店铺地区数量（按省级地区分组）")
    @GetMapping("/queryStoreInfoAreaStatistics")
    List<StoreInfoAreaStatistics> queryStoreInfoAreaStatistics(@RequestParam("startTime") String startTime,
                                                               @RequestParam("endTime") String endTime){
        return areaStatisticsService.queryStoreInfoAreaStatistics(startTime, endTime);
    }
}
