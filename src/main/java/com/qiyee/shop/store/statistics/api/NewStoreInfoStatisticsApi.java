package com.qiyee.shop.store.statistics.api;

import com.qiyee.shop.entity.customer.statistics.NewStoreInfoStatistics;
import com.qiyee.shop.store.statistics.service.NewStoreInfoStatisticsService;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/store/statistics")
@RestController
@Api("店铺地区统计服务接口")
public class NewStoreInfoStatisticsApi {

    @Autowired
    private NewStoreInfoStatisticsService statisticsService;

    /**
     * 统计新增店铺数量（按日期分组）
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 返回新增店铺数量
     */
    @ApiOperation(value = "统计新增店铺数量（按日期分组）", notes = "统计新增店铺数量（按日期分组）")
    @GetMapping("/queryNewStoreInfoStatistics")
    List<NewStoreInfoStatistics> queryNewStoreInfoStatistics(@RequestParam("startTime") String startTime,
                                                             @RequestParam("endTime") String endTime){
        return statisticsService.queryNewStoreInfoStatistics(startTime, endTime);
    }

    /**
     * 分页统计新增店铺数量（按日期分组）
     *
     * @param pageHelper 分页帮助类
     * @param startTime  开始时间
     * @param endTime    结束时间
     * @return 返回新增店铺数量（带分页）
     */
    @ApiOperation(value = "分页统计新增店铺数量（按日期分组）", notes = "分页统计新增店铺数量（按日期分组）")
    @PostMapping("/queryNewStoreInfoStatisticsWithPage")
    PageHelper<NewStoreInfoStatistics> queryNewStoreInfoStatisticsWithPage(@RequestBody PageHelper<NewStoreInfoStatistics> pageHelper,
                                                                           @RequestParam("startTime") String startTime,
                                                                           @RequestParam("endTime") String endTime){
        return statisticsService.queryNewStoreInfoStatisticsWithPage(pageHelper, startTime, endTime);
    }
}
