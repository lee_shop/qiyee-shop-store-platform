package com.qiyee.shop.store.statistics.service.impl;

import com.qiyee.shop.entity.customer.statistics.StoreInfoAreaStatistics;
import com.qiyee.shop.store.statistics.service.StoreInfoAreaStatisticsService;
import com.qiyee.shop.store.store.mapper.StoreInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 店铺地区统计服务接口实现类
 *
 * @author administrator created on 2019/4/11
 */
@Service
@Slf4j
public class StoreInfoAreaStatisticsServiceImpl implements StoreInfoAreaStatisticsService {

    /**
     * 注入店铺数据库接口
     */
    @Autowired
    private StoreInfoMapper storeInfoMapper;

    @Override
    public List<StoreInfoAreaStatistics> queryStoreInfoAreaStatistics(String startTime, String endTime) {
        log.debug("queryStoreInfoAreaStatistics and startTime :{} \r\n endTime :{}", startTime, endTime);
        Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        return storeInfoMapper.queryStoreInfoAreaStatistics(params);
    }

}
