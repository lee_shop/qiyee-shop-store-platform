package com.qiyee.shop.store.paytype.service.impl;

import com.qiyee.shop.entity.system.storepaytype.StorePayType;
import com.qiyee.shop.store.paytype.mapper.StorePayTypeMapper;
import com.qiyee.shop.store.paytype.service.StorePayTypeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gaozhuwei on 18/4/10.
 * 门店支付类型接口实现
 */
@Service
@RequiredArgsConstructor
public class StorePayTypeServiceImpl implements StorePayTypeService {

    /**
     * 注入门店支付类型数据库
     */
    private final StorePayTypeMapper storePayTypeMapper;

    /**
     * 调试日志
     */
    Logger logger = LoggerFactory.getLogger(StorePayTypeServiceImpl.class);

    /**
     * 查询门店支付类型
     * @param id 支付类型ID
     * @return 返回门店支付集合
     */
    @Override
    public List<StorePayType> queryStorePayType(String id) {
        logger.debug("queryStorePayType and id :{}",id);
        return storePayTypeMapper.queryStorePayType(id);
    }

    /**
     * 添加门店支付类型
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    @Override
    public int addStorePayType(String name) {
        logger.debug("addStorePayType and name :{}",name);
        return storePayTypeMapper.addStorePayType(name);
    }

    /**
     * 删除门店支付类型
     * @param id 门店支付类型名称id
     * @return 1成功 0失败
     */
    @Override
    public int delStorePayType(long id) {
        logger.debug("delStorePayType and id :{}",id);
        return storePayTypeMapper.delStorePayType(id);
    }

    /**
     * 修改门店支付类型
     * @param id 门店支付类型名称id
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    @Override
    public int updStorePayType(long id, String name) {
        logger.debug("updStorePayType and id:{} \n\n name:{}",id,name);
        Map<String,Object> params = new HashMap<>(2);
        params.put("id",id);
        params.put("name",name);
        return storePayTypeMapper.updStorePayType(params);
    }
}
