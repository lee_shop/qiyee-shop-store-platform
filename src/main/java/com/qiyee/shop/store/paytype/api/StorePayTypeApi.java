package com.qiyee.shop.store.paytype.api;

import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.entity.system.storepaytype.StorePayType;
import com.qiyee.shop.store.paytype.service.StorePayTypeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * Created by gaozhuwei on 18/4/10.
 * 门店支付类型
 */
@RequestMapping("/store/paytype")
@RestController
@Api("门店支付类型服务接口")
public class StorePayTypeApi {

    @Autowired
    private StorePayTypeService storePayTypeService;

    /**
     * 查询门店支付类型
     * @param id 支付类型ID
     * @return 返回门店支付集合
     */
    @GetMapping("/queryStorePayType")
    @ApiOperation(value = "查询门店支付类型", notes = "查询门店支付类型（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "id", value = "支付类型ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店支付类型", response = Coupon.class)
    })
    public List<StorePayType> queryStorePayType(@RequestParam(value = "id",required = false) String id){
        return storePayTypeService.queryStorePayType(id);
    }

    /**
     * 添加门店支付类型
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    @PostMapping("/addStorePayType")
    @ApiOperation(value = "添加门店支付类型", notes = "添加门店支付类型（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "name", value = "门店支付类型名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "添加门店支付类型", response = Coupon.class)
    })
    public int addStorePayType(@RequestParam(value = "name") String name){
        return storePayTypeService.addStorePayType(name);
    }

    /**
     * 删除门店支付类型
     * @param id 门店支付类型名称id
     * @return 1成功 0失败
     */
    @PutMapping("/delStorePayType/{id}")
    @ApiOperation(value = "删除门店支付类型", notes = "删除门店支付类型（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "门店支付类型名称id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除门店支付类型", response = Coupon.class)
    })
    public int delStorePayType(@PathVariable(value = "id") long id){
        return storePayTypeService.delStorePayType(id);
    }

    /**
     * 修改门店支付类型
     * @param id 门店支付类型名称id
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    @PutMapping("/updStorePayType/{id}")
    @ApiOperation(value = "修改门店支付类型", notes = "修改门店支付类型（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "门店支付类型名称id"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "name", value = "门店支付类型名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "修改门店支付类型", response = Coupon.class)
    })
    public int updStorePayType(@PathVariable(value = "id") long id,@RequestParam(value = "name") String name){
        return storePayTypeService.updStorePayType(id,name);
    }
}
