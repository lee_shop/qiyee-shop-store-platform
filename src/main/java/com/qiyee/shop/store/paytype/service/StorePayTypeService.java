package com.qiyee.shop.store.paytype.service;

import com.qiyee.shop.entity.system.storepaytype.StorePayType;

import java.util.List;

/**
 * Created by gaozhuwei on 18/4/10.
 * 门店支付类型接口
 */
public interface StorePayTypeService {

    /**
     * 查询门店支付类型
     * @param id 支付类型ID
     * @return 返回门店支付集合
     */
    List<StorePayType> queryStorePayType(String id);

    /**
     * 添加门店支付类型
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    int addStorePayType(String name);

    /**
     * 删除门店支付类型
     * @param id 门店支付类型名称id
     * @return 1成功 0失败
     */
    int delStorePayType(long id);

    /**
     * 修改门店支付类型
     * @param id 门店支付类型名称id
     * @param name 门店支付类型名称
     * @return 1成功 0失败
     */
    int updStorePayType(long id,String name);
}
