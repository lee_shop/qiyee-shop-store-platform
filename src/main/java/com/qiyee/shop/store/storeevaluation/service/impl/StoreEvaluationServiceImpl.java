package com.qiyee.shop.store.storeevaluation.service.impl;

import com.qiyee.shop.entity.customer.openstore.StoreInfo;
import com.qiyee.shop.entity.customer.storeevaluation.StoreEvaluation;
import com.qiyee.shop.store.store.service.StoreInfoService;
import com.qiyee.shop.store.util.PageHelper;
import com.qiyee.shop.store.storeevaluation.mapper.StoreEvaluationMapper;
import com.qiyee.shop.store.storeevaluation.service.StoreEvaluationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by gaozhuwei on 18/4/9.
 * 门店评价服务接口实现
 */
@Service
public class StoreEvaluationServiceImpl implements StoreEvaluationService {

    /**
     * 注入店铺服务接口
     */
    @Autowired
    private StoreInfoService storeInfoService;

    /**
     * 注入门店评价数据接口
     */
    @Autowired
    private StoreEvaluationMapper storeEvaluationMapper;

    /**
     * 调试日志
     */
    private Logger logger = LoggerFactory.getLogger(StoreEvaluationServiceImpl.class);

    /**
     * 新增门店评分
     *
     * @param storeEvaluation 门店评分
     * @return 成功1 失败0
     */
    @Override
    public int addStoreEvaluation(StoreEvaluation storeEvaluation) {
        logger.debug("addStoreEvaluation and addStoreEvaluation:{}", storeEvaluation);

        if (Objects.isNull(storeEvaluation)) {
            logger.error("addStoreEvaluation fail due to storeEvaluation is null...");
            return 0;
        }

        // 修改门店订单评价状态为已评价
        storeEvaluationMapper.updEvaluationStatus(storeEvaluation.getOrderCode());

        return storeEvaluationMapper.addStoreEvaluation(storeEvaluation);
    }

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param userName   评分人
     * @param storeId    门店ID
     * @return 门店评分信息
     */
    @Override
    public PageHelper<StoreEvaluation> queryStoreEvaluation(PageHelper<StoreEvaluation> pageHelper, String userName, long storeId) {
        logger.debug("queryStoreEvaluation and pageHelper:{} \n\n userName:{}", pageHelper, userName);
        // 获得查询参数
        Map<String, Object> params = new HashMap<>(2);
        params.put("userName", userName);
        params.put("storeId", storeId);
        return pageHelper.setListDates(storeEvaluationMapper.queryStoreEvaluation(pageHelper.getQueryParams(params, storeEvaluationMapper.queryStoreEvaluationCount(params))));
    }

    /**
     * 分页查询门店评分列表页
     *
     * @param pageHelper 分页辅助类
     * @param customerId 会员ID
     * @return 门店评分列表页
     */
    @Override
    public PageHelper<StoreEvaluation> selStoreEvaluationList(PageHelper<StoreEvaluation> pageHelper, long customerId) {
        logger.debug("selStoreEvaluationList and pageHelper:{} \n\n customerId:{}", pageHelper, customerId);
        // 获得查询参数
        Map<String, Object> params = new HashMap<>(1);
        params.put("customerId", customerId);
        return pageHelper.setListDates(storeEvaluationMapper.selStoreEvaluationList(pageHelper.getQueryParams(params, storeEvaluationMapper.selStoreEvaluationListCount(params))));
    }

    /**
     * 查询门店评分
     *
     * @param storeId 门店id
     * @return 评分
     */
    @Override
    public long queryStoreAveScore(long storeId) {
        logger.debug("queryStoreAveScore and storeId:{}", storeId);
        return storeEvaluationMapper.queryStoreAveScore(storeId);
    }

    /**
     * 查询门店评分详情
     *
     * @param orderCode  订单编号
     * @param customerId 会员ID
     * @return 评分信息
     */
    @Override
    public StoreEvaluation queryStoreInfoDetail(String orderCode, long customerId) {
        logger.debug("queryStoreInfoDetail and orderCode:{} \n\n customerId:{}", orderCode, customerId);
        // 获得查询参数
        Map<String, Object> params = new HashMap<>(2);
        params.put("orderCode", orderCode);
        params.put("customerId", customerId);

        // 店铺评价
        StoreEvaluation storeEvaluation = storeEvaluationMapper.queryStoreInfoDetail(params);

        if (Objects.isNull(storeEvaluation)) {
            return storeEvaluation;
        }

        // 店铺信息
        StoreInfo storeInfo = storeInfoService.selStoreInfo(storeEvaluation.getStoreId());

        if (Objects.nonNull(storeInfo)) {
            storeEvaluation.setAvatarPicture(storeInfo.getAvatarPicture());
            storeEvaluation.setStoreName(storeInfo.getStoreName());
            storeEvaluation.setCompanyAddress(storeInfo.getCompanyAddress());
            storeEvaluation.setPhone(storeInfo.getContactPhone());
            storeEvaluation.setBusinessTime(storeInfo.getBusinessTime());
            storeEvaluation.setBusRoute(storeInfo.getBusRoutes());
            storeEvaluation.setAvgScore(storeInfo.getAveScore());
        }

        return storeEvaluation;
    }

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param storeName 店铺名称
     * @return 门店评分信息
     */
    @Override
    public PageHelper<StoreEvaluation> queryAdminStoreEvaluation(PageHelper<StoreEvaluation> pageHelper, String storeName) {
        // 获得查询参数
        Map<String, Object> params = new HashMap<>(1);
        params.put("storeName", storeName);
        return pageHelper.setListDates(storeEvaluationMapper.queryAdminStoreEvaluationList(pageHelper.getQueryParams(params, storeEvaluationMapper.queryAdminStoreEvaluationCount(params))));
    }
}
