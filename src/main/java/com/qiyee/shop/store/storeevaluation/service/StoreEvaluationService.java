package com.qiyee.shop.store.storeevaluation.service;

import com.qiyee.shop.entity.customer.storeevaluation.StoreEvaluation;
import com.qiyee.shop.store.util.PageHelper;

/**
 * 门店评价服务接口
 */
public interface StoreEvaluationService {

    /**
     * 新增门店评分
     *
     * @param storeEvaluation 门店评分
     * @return 成功1 失败0
     */
    int addStoreEvaluation(StoreEvaluation storeEvaluation);

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param userName 评分人
     * @param storeId 门店ID
     * @return 门店评分信息
     */
    PageHelper<StoreEvaluation> queryStoreEvaluation(PageHelper<StoreEvaluation> pageHelper, String userName, long storeId);

    /**
     * 分页查询门店评分列表页
     * @param pageHelper 分页辅助类
     * @param customerId 会员ID
     *
     * @return 门店评分列表页
     */
    PageHelper<StoreEvaluation> selStoreEvaluationList(PageHelper<StoreEvaluation> pageHelper, long customerId);

    /**
     * 查询门店评分
     *
     * @param storeId 门店id
     * @return 评分
     */
    long queryStoreAveScore(long storeId);

    /**
     * 查询门店评分详情
     * @param orderCode 订单编号
     * @param customerId 会员ID
     * @return 评分信息
     */
    StoreEvaluation queryStoreInfoDetail(String orderCode, long customerId);

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param storeName 店铺名称
     * @return 门店评分信息
     */
    PageHelper<StoreEvaluation> queryAdminStoreEvaluation(PageHelper<StoreEvaluation> pageHelper, String storeName);
}
