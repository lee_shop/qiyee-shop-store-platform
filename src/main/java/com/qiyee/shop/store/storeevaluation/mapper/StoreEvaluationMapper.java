package com.qiyee.shop.store.storeevaluation.mapper;

import com.qiyee.shop.entity.customer.storeevaluation.StoreEvaluation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 门店评价mapper
 */
public interface StoreEvaluationMapper {

    /**
     * 新增门店评分
     *
     * @param storeEvaluation 门店评分
     * @return 成功1 失败0
     */
    int addStoreEvaluation(StoreEvaluation storeEvaluation);

    /**
     * 查询门店评分总数
     *
     * @param params 参数条件
     * @return 行数
     */
    int queryStoreEvaluationCount(Map<String, Object> params);

    /**
     * 查询门店评分
     *
     * @param params 参数条件
     * @return 返回门店评分
     */
    List<StoreEvaluation> queryStoreEvaluation(Map<String, Object> params);

    /**
     * 查询门店评分列表总数
     *
     * @param params 参数
     * @return 行数
     */
    int selStoreEvaluationListCount(Map<String, Object> params);

    /**
     * 查询门店评分列表
     *
     * @param params 参数条件
     * @return 返回门店评分列表
     */
    List<StoreEvaluation> selStoreEvaluationList(Map<String, Object> params);


    /**
     * 修改门店订单评价状态为已评价
     * @param orderCode 订单号
     * @return 1成功 0失败
     */
    int updEvaluationStatus(String orderCode);

    /**
     * 查询门店评分
     *
     * @param storeId 门店id
     * @return 评分
     */
    long queryStoreAveScore(@Param("storeId") long storeId);

    /**
     * 查询门店评分详情
     * @param params 参数条件
     * @return 门店评分详情
     */
    StoreEvaluation queryStoreInfoDetail(Map<String, Object> params);

    /**
     * 查询门店评分总数
     * @return 行数
     */
    int queryAdminStoreEvaluationCount(Map<String, Object> params);


    /**
     * 分页查询门店评分
     * @param params 参数条件
     * @return 返回门店评分
     */
    List<StoreEvaluation> queryAdminStoreEvaluationList(Map<String, Object> params);

}
