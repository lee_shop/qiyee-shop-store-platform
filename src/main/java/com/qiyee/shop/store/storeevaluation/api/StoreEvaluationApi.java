package com.qiyee.shop.store.storeevaluation.api;

import com.qiyee.shop.entity.customer.storeevaluation.StoreEvaluation;
import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.store.storeevaluation.service.StoreEvaluationService;
import com.qiyee.shop.store.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/store/evaluation")
@RestController
@Api("门店评分接口")
public class StoreEvaluationApi {

    @Autowired
    private StoreEvaluationService storeEvaluationService;

    /**
     * 新增门店评分
     *
     * @param storeEvaluation 门店评分
     * @return 成功1 失败0
     */
    @PostMapping("/addStoreEvaluation")
    @ApiOperation(value = "新增门店评分", notes = "新增门店评分")
    @ApiResponses({
            @ApiResponse(code = 200, message = "新增门店评分", response = Coupon.class)
    })
    public int addStoreEvaluation(@RequestBody StoreEvaluation storeEvaluation){
        return storeEvaluationService.addStoreEvaluation(storeEvaluation);
    }

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param userName 评分人
     * @param storeId 门店ID
     * @return 门店评分信息
     */
    @PostMapping("/queryStoreEvaluation/{storeId}")
    @ApiOperation(value = "分页查询门店评分", notes = "分页查询门店评分")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "userName", value = "评分人"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "门店ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "分页查询门店评分", response = Coupon.class)
    })
    public PageHelper<StoreEvaluation> queryStoreEvaluation(@RequestBody PageHelper<StoreEvaluation> pageHelper,
                                                            @RequestParam("userName") String userName,
                                                            @PathVariable("storeId") long storeId){
        return storeEvaluationService.queryStoreEvaluation(pageHelper,userName,storeId);
    }

    /**
     * 分页查询门店评分列表页
     * @param pageHelper 分页辅助类
     * @param customerId 会员ID
     *
     * @return 门店评分列表页
     */
    @PostMapping("/selStoreEvaluationList/{customerId}")
    @ApiOperation(value = "分页查询门店评分", notes = "分页查询门店评分")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "customerId", value = "会员ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "分页查询门店评分", response = Coupon.class)
    })
    public PageHelper<StoreEvaluation> selStoreEvaluationList(@RequestBody PageHelper<StoreEvaluation> pageHelper,
                                                              @PathVariable("customerId") long customerId){
        return storeEvaluationService.selStoreEvaluationList(pageHelper,customerId);
    }

    /**
     * 查询门店评分
     *
     * @param storeId 门店id
     * @return 评分
     */
    @GetMapping("/queryStoreAveScore/{storeId}")
    @ApiOperation(value = "查询门店评分", notes = "查询门店评分")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "门店id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店评分", response = Coupon.class)
    })
    public long queryStoreAveScore(@PathVariable("storeId") long storeId){
        return storeEvaluationService.queryStoreAveScore(storeId);
    }

    /**
     * 查询门店评分详情
     * @param orderCode 订单编号
     * @param customerId 会员ID
     * @return 评分信息
     */
    @GetMapping("/queryStoreInfoDetail/{customerId}")
    @ApiOperation(value = "查询门店评分详情", notes = "查询门店评分详情")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "orderCode", value = "订单编号"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店评分详情", response = Coupon.class)
    })
    public StoreEvaluation queryStoreInfoDetail(@RequestParam("orderCode") String orderCode,
                                                @PathVariable("orderCode") long customerId){
        return storeEvaluationService.queryStoreInfoDetail(orderCode,customerId);
    }

    /**
     * 分页查询门店评分
     *
     * @param pageHelper 分页辅助类
     * @param storeName 店铺名称
     * @return 门店评分信息
     */
    @PostMapping("/queryAdminStoreEvaluation")
    @ApiOperation(value = "分页查询门店评分", notes = "分页查询门店评分")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店评分详情", response = Coupon.class)
    })
    public PageHelper<StoreEvaluation> queryAdminStoreEvaluation(@RequestBody PageHelper<StoreEvaluation> pageHelper,
                                                                 @RequestParam("storeName") String storeName){
        return storeEvaluationService.queryAdminStoreEvaluation(pageHelper,storeName);
    }
}
