package com.qiyee.shop.store.store.mapper;

import com.qiyee.shop.entity.customer.openstore.OnSaleStoreQueryParam;
import com.qiyee.shop.entity.customer.openstore.PCStoreListInfo;
import com.qiyee.shop.entity.customer.openstore.StoreInfo;
import com.qiyee.shop.entity.customer.statistics.NewStoreInfoStatistics;
import com.qiyee.shop.entity.customer.statistics.StoreInfoAreaStatistics;
import com.qiyee.shop.entity.order.area.City;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 店铺信息mapper
 *
 * @author administrator on 2017/6/13.
 */
public interface StoreInfoMapper {
    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeId 店铺id
     * @return 店铺信息
     */
    StoreInfo queryStoreInfo(long storeId);

	/**
	 * 根据店铺id查询店铺名称
	 *
	 * @param storeId 店铺id
	 * @return 店铺名称
	 */
	String queryStoreName(long storeId);

    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeName 店铺id
     * @return 店铺信息
     */
    List<StoreInfo> listStoreByStoreName(String storeName);
    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeIds 店铺id
     * @return 店铺信息
     */
    List<StoreInfo> listStoreByStoreIds(@Param("list") List<Long> storeIds) ;

    /**
     * 根据店铺id查询店铺审核通过的信息
     *
     * @param storeId 店铺id
     * @return 店铺审核通过的信息
     */
    StoreInfo queryAuditPassStoreInfo(long storeId);

    /**
     * 开店-添加店铺信息(第一步)
     *
     * @param storeInfo 店铺信息实体类
     * @return 添加返回码
     */
    int addStoreInfo(StoreInfo storeInfo);


    /**
     * 开店-添加店铺信息(第一步)
     *
     * @param storeInfo 店铺信息实体类
     * @return 添加返回码
     */
    int addPCStoreInfo(StoreInfo storeInfo);

    /**
     * 开店-编辑店铺信息(第二步)
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editStoreInfo(StoreInfo storeInfo);

    /**
     * 开店-编辑店铺信息(第二步)
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editPCStoreInfo(StoreInfo storeInfo);

    /**
     * 编辑店铺名称
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editStoreName(StoreInfo storeInfo);

    /**
     * 根据店铺名称查询店铺
     *
     * @param storeName 店铺名称
     * @return 店铺信息实体类
     */
    StoreInfo queryStoreInfoByName(String storeName);

    /**
     * 查询已审核/未审核商家个数
     *
     * @param map 查询参数
     * @return 个数
     */
    int queryStoreInfoForAuditListCount(Map<String, Object> map);

    /**
     * 查询已审核/未审核商家集合
     *
     * @param map 查询参数
     * @return 已审核/未审核商家集合
     */
    List<StoreInfo> queryStoreInfoForAuditList(Map<String, Object> map);

    /**
     * 编辑店铺有效期,结算周期,是否关店
     *
     * @param storeInfo 店铺信息
     * @return 编辑返回码
     */
    int editStoreTimeAndIsClose(StoreInfo storeInfo);

    /**
     * 通过商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    int passStoreAudit(StoreInfo storeInfo);

    /**
     * 拒绝商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    int refuseStoreAudit(StoreInfo storeInfo);

    /**
     * 删除商家
     *
     * @param id 商家id
     * @return 成功返回1，失败返回0
     */
    int deleteStore(long id);

    /**
     * 编辑店铺信息-客服QQ
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editStoreInfoForServiceQQ(StoreInfo storeInfo);

    /**
     * 编辑店铺信息-公司信息
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editStoreInfoForCompanyInfo(StoreInfo storeInfo);

    /**
     * 编辑店铺信息-银行信息
     *
     * @param storeInfo 店铺信息实体类
     * @return 编辑返回码
     */
    int editStoreInfoForBankInfo(StoreInfo storeInfo);

    /**
     * 根据条件搜索店铺
     *
     * @param params 搜索条件：排序条件，关键字
     * @return 店铺集合
     */
    List<StoreInfo> queryStoreInfoForSearch(Map params);

    /**
     * 根据条件搜索店铺数量
     *
     * @param params 搜索条件：排序条件，关键字
     * @return 数量
     */
    int queryStoreInfoForSearchCount(Map params);

    /**
     * 统计新增店铺数量（按日期分组）
     *
     * @param params 查询参数
     * @return 返回按日期分组的新增店铺数量
     */
    List<NewStoreInfoStatistics> queryNewStoreInfoStatistics(Map<String, Object> params);

    /**
     * 统计新增店铺数量（按日期分组）总组数
     *
     * @param params 查询参数
     * @return 返回总组数
     */
    int queryNewStoreInfoStatisticsCount(Map<String, Object> params);

    /**
     * 分页统计新增店铺数量（按日期分组）
     *
     * @param params 查询参数
     * @return 返回按日期分组的新增店铺数量（带分页）
     */
    List<NewStoreInfoStatistics> queryNewStoreInfoStatisticsWithPage(Map<String, Object> params);

    /**
     * 统计店铺地区数量（按省级日期分组）
     *
     * @param params 查询参数
     * @return 返回店铺地区数量统计
     */
    List<StoreInfoAreaStatistics> queryStoreInfoAreaStatistics(Map<String, Object> params);

    /**
     * 批量关闭店铺
     *
     * @param ids 店铺id集合
     * @return >0成功，否则失败
     */
    int closeStores(List<Long> ids);

    /**
     * 开启店铺(门店用)
     *
     * @param storeId 店铺id
     * @return 1成功，否则失败
     */
    int openStoreForOutLetStore(@Param("storeId") long storeId);

    /**
     * 根据店铺名查询店铺数量
     *
     * @param params 查询参数
     * @return 店铺数量
     */
    int queryStoreCountByStoreName(Map<String, Object> params);

    /**
     * 根据公司名查询店铺数量
     *
     * @param params 查询参数
     * @return 店铺数量
     */
    int queryStoreCountByCompanyName(Map<String, Object> params);

    /**
     * 添加店铺
     *
     * @param storeInfo 店铺信息实体
     * @return 店铺id
     */
    long addStore(StoreInfo storeInfo);

    /**
     * 查找一个在售门店
     *
     * @param skuId  单品id
     * @param cityId 市id
     * @return 门店信息
     */
    StoreInfo queryOneOnSaleStore(@Param("skuId") String skuId, @Param("cityId") long cityId);

    /**
     * 查找在售门店
     *
     * @param skuId  单品id
     * @param cityId 市id
     * @return 门店信息集合
     */
    List<StoreInfo> queryOnSaleStoreList(@Param("skuId") String skuId, @Param("cityId") long cityId);

    /**
     * 查找一个在售门店(根据经纬度距离排序)
     *
     * @param onSaleStoreQueryParam 在售门店搜索参数
     * @return 门店信息
     */
    StoreInfo queryOneOnSaleStoreByCoordinate(OnSaleStoreQueryParam onSaleStoreQueryParam);

    /**
     * 查找在售门店(根据经纬度距离排序)
     *
     * @param onSaleStoreQueryParam 在售门店搜索参数
     * @return 门店信息集合
     */
    List<StoreInfo> queryOnSaleStoreListByCoordinate(OnSaleStoreQueryParam onSaleStoreQueryParam);

    /**
     * 根据定位查询附近门店列表
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  距离
     * @return 门店信息
     */
    List<StoreInfo> queryNearByStoreList(@Param("longitude") BigDecimal longitude, @Param("latitude") BigDecimal latitude, @Param("distance") int distance);

    /**
     * 查询门店列表
     *
     * @param params 参数
     * @return 返回门店列表
     */
    List<StoreInfo> queryStoreInfoList(Map<String, Object> params);

    /**
     * 查询门店列表总数
     *
     * @param params 参数
     * @return 返回门店列表总数
     */
    int queryStoreInfoListCount(Map<String, Object> params);

    /**
     * 根据定位查询附近店铺列表
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  距离
     * @return 店铺信息
     */
    List<StoreInfo> queryNearStoreList(@Param("longitude") BigDecimal longitude, @Param("latitude") BigDecimal latitude, @Param("distance") int distance);


    /**
     * 查询已审核/未审核商家个数
     *
     * @param map 查询参数
     * @return 个数
     */
    int queryPCStoreInfoForAuditListCount(Map<String, Object> map);

    /**
     * 查询已审核/未审核商家集合
     *
     * @param map 查询参数
     * @return 已审核/未审核商家集合
     */
    List<PCStoreListInfo> queryPCStoreInfoForAuditList(Map<String, Object> map);

	/**
	 * 根据城市id查询城市对象
	 * @param id 城市id
	 * @return 城市实体对象
	 */
	City queryCityById(long id);

	/**
	 * 发起直播时分页搜索所有店铺总数量
	 * @param params 查询参数
	 * @return
	 */
	int queryStoreInfoForLiveSearchCount(Map<String, Object> params);

	/**
	 * 发起直播时分页搜索所有店铺
	 * @param queryParams 查询参数
	 * @return
	 */
	List<StoreInfo> queryStoreInfoForLiveSearch(Map<String, Object> queryParams);
}
