package com.qiyee.shop.store.store.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.qiyee.shop.dto.product.req.brand.QueryCustomBrandByStoreIdAndStatusReqDTO;
import com.qiyee.shop.dto.product.req.brand.QueryStoreBrandsReqDTO;
import com.qiyee.shop.dto.product.req.spu.UpdateShelvesStatusByStoreIdsReqDTO;
import com.qiyee.shop.entity.customer.customer.Customer;
import com.qiyee.shop.entity.customer.openstore.*;
import com.qiyee.shop.entity.exception.BizException;
import com.qiyee.shop.entity.openstore.AnchorSell;
import com.qiyee.shop.entity.openstore.AnchorSellVo;
import com.qiyee.shop.entity.order.area.City;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.brand.BrandApply;
import com.qiyee.shop.entity.product.category.StoreSignedCategory;
import com.qiyee.shop.entity.system.storerole.RoleAndCustomer;
import com.qiyee.shop.store.feign.client.EsClient;
import com.qiyee.shop.store.feign.client.ProductBaseClient;
import com.qiyee.shop.store.feign.client.ProductClient;
import com.qiyee.shop.store.feign.comment.StoreCommentClientService;
import com.qiyee.shop.store.feign.customer.CustomerService;
import com.qiyee.shop.store.feign.es.EsFeign;
import com.qiyee.shop.store.feign.site.StoreTemplateService;
import com.qiyee.shop.store.store.mapper.AnchorSellMapper;
import com.qiyee.shop.store.store.mapper.StoreInfoMapper;
import com.qiyee.shop.store.store.service.StoreInfoService;
import com.qiyee.shop.store.storeevaluation.service.StoreEvaluationService;
import com.qiyee.shop.store.storerole.service.StoreRoleService;
import com.qiyee.shop.util.CommonConstant;
import com.qiyee.shop.util.CommonResponse;
import com.qiyee.shop.util.PageHelper;
import com.qiyee.shop.util.ServiceException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 开店流程service
 *
 * @author administrator on 2017/6/13.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class StoreInfoServiceImpl implements StoreInfoService {

    /**
     * 注入es服务接口
     */
    @Resource
    private EsClient esClient;

    /**
     * EsFeign
     */
    private final EsFeign esFeign;

    /**
     * 品牌service
     */
    private final ProductClient productClient;

    /**
     * 注入店铺信息mapper
     */
    private final StoreInfoMapper storeInfoMapper;

    /**
     * 注入主播开售mapper
     */
    private final AnchorSellMapper anchorSellMapper;

    /**
     * 注入会员service
     */
    private final CustomerService customerService;

    /**
     * 注入店铺角色service
     */
    private final StoreRoleService storeRoleService;


    /**
     * 品牌申请service
     */
    private final ProductBaseClient productBaseClient;

    /**
     * 注入店铺首页模板服务
     */
    private final StoreTemplateService storeTemplateService;

    /**
     * 注入店铺评论服务
     */
    private final StoreCommentClientService storeCommentService;


    /**
     * 注入门店评价服务
     */
    @Autowired
    private StoreEvaluationService storeEvaluationService;

    /**
     * 调试日志
     */
    private Logger logger = LoggerFactory.getLogger(StoreInfoServiceImpl.class);

    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeId 店铺id
     * @return 店铺信息
     */
    @Override
    public StoreInfo queryStoreInfo(Long storeId) {
        logger.debug("queryAuditPassStoreInfo and storeId:{}", storeId);

        if (storeId == 0) {
            StoreInfo storeInfo = new StoreInfo();
            storeInfo.setId(0);
            storeInfo.setStoreName("商城自营");
            return storeInfo;
        }

        return storeInfoMapper.queryStoreInfo(storeId);
    }

    @Override
    public Map<String, Object> queryStoreNameByStoreId(Long storeId) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 0);
        if (storeId == 0) {
            map.put("storeName", "商城自营");
            return map;
        }

        map.put("storeName", storeInfoMapper.queryStoreName(storeId));
        return map;
    }

    /**
     * 根据店铺id查询店铺信息
     *
     * @return 店铺信息
     */
    @Override
    public List<StoreInfo> listStoreByStoreName(String storeName) {
        return storeInfoMapper.listStoreByStoreName(storeName);
    }

    /**
     * 根据店铺id查询店铺信息
     *
     * @return 店铺信息
     */
    @Override
    public List<StoreInfo> listStoreByStoreIds(List<Long> storeIds) {
        return storeInfoMapper.listStoreByStoreIds(storeIds);
    }


    /**
     * 查询开店时填写的信息
     *
     * @param customerId 会员ID
     * @return 店铺信息
     */
    @Override
    public StoreInfo findOpenStoreInfo(long customerId) {
        logger.debug("findOpenStoreInfo and customerId:{}", customerId);
        Customer customer = customerService.queryCustomerWithNoPasswordById(customerId);
        StoreInfo returnStoreInfo = new StoreInfo();
        returnStoreInfo.setDelFlag("1");
        StoreInfo storeInfo = queryStoreInfo(customer.getStoreId());
        return storeInfo == null ? returnStoreInfo : storeInfo;
    }

    /**
     * 开店处理店铺信息
     *
     * @param storeInfo  店铺实体类
     * @param customerId 会员ID
     * @return 返回值跳转页面  0 出错 1 下一页 2店铺首页 3 拒绝通过页 4 登录页
     */
    @Override
    public int dealStoreInfo(StoreInfo storeInfo, long customerId) {
        logger.debug("dealStoreInfo and storeInfo:{}\r\n customerId:{}", storeInfo, customerId);
        Customer customer = customerService.queryCustomerWithNoPasswordById(customerId);
        com.qiyee.shop.entity.customer.openstore.StoreInfo queryStoreInfo = storeInfoMapper.queryStoreInfo(customer.getStoreId());
        storeInfo.setStatus("0");
        //店铺信息有无信息

        //无信息   新增
        if (Objects.isNull(queryStoreInfo)) {
            //新增
            if (checkCompanyNameExist(storeInfo.getCompanyName(), CommonConstant.QUERY_WITH_NO_STORE) > 0) {
                logger.error("dealStoreInfo fail :");
                return -1;
            }
            if (storeInfoMapper.addStoreInfo(storeInfo) == 1) {
                //更新会员表中的数据
                Customer reCustomer = new Customer();
                reCustomer.setStoreId(storeInfo.getId());
                reCustomer.setType("2");
                reCustomer.setId(customer.getId());
                //更新会员表中的storeI的和type
                return customerService.updateStoreIdAndType(reCustomer);
            }
            return 0;
        }

        //有信息 编辑
        storeInfo.setId(queryStoreInfo.getId());
        if (checkCompanyNameExist(storeInfo.getCompanyName(), storeInfo.getId()) > 0) {
            logger.error("dealStoreInfo fail :");
            return -1;
        }
        //编辑
        return storeInfoMapper.editStoreInfo(storeInfo);

    }

    @Override
    public PCStoreReview dealPCStoreInfo(StoreInfo storeInfo, long customerId) {
		logger.info("dealPCStoreInfo and storeInfo:{}\r\n customerId:{}", JSONObject.toJSONString(storeInfo),
				customerId);
		Customer customer = new Customer();
		if (customerId > 0) {
			customer = customerService.queryCustomerWithNoPasswordById(customerId);
			storeInfo.setStatus("1");
		}
		int queryindex = 0;
		if (storeInfo.getId() == 0) {
			// 新增
			if (storeInfoMapper.addPCStoreInfo(storeInfo) == 1) {
				//更新会员表中的数据
				Customer reCustomer = new Customer();
				reCustomer.setStoreId(storeInfo.getId());
				reCustomer.setType("2");
				reCustomer.setId(customerId);
				//更新会员表中的storeI的和type
				queryindex = customerService.updateStoreIdAndType(reCustomer);
			}
		} else {
			// 修改
			queryindex = storeInfoMapper.editPCStoreInfo(storeInfo);
			if (queryindex == 1 && "2".equals(storeInfo.getStatus())) {
				try {
					this.passStoreAudit(storeInfo);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
		if (queryindex == 1) {
			return PCStoreReview.buildUnderReview(customer.getUserName(), storeInfo.getId(), storeInfo.getStoreName());
		}
		return null;
	}

    /**
     * 开店-处理店铺经营信息
     *
     * @param customerId  会员ID
     * @param storeName   店铺名称
     * @param categoryIds 分类ids
     * @param brandIds    品牌ids
     * @param brands      自定义品牌集合
     * @return 返回码 1处理成功
     */
    @Override
    @Transactional
    public int dealStoreBusinessInfo(long customerId, String storeName, long[] categoryIds, long[] brandIds, List<Brand> brands) throws Exception {
        logger.debug("dealStoreBusinessInfo and customerId:{}\r\n storeName:{}\r\n categoryIds:{}\r\n categoryIds:{}\r\n" +
                "brandIds:{}\r\n brands:{}", customerId, storeName, categoryIds, brandIds, brands);
        StoreInfo storeInfo = storeInfoMapper.queryStoreInfoByName(storeName);
        Customer customer = customerService.queryCustomerWithNoPasswordById(customerId);
        if (!Objects.isNull(storeInfo) && customer.getStoreId() != storeInfo.getId()) {
            return -1;
        }
        long storeId = customer.getStoreId();
        String name = customer.getUserName();
        saveOtherStoreInfo(categoryIds, brandIds, brands, storeId, name);
        //更新店铺名称
        return storeInfoMapper.editStoreName(new StoreInfo().getStoreInfoToEditStoreName(storeId, "1", storeName));
    }

    /**
     * 保存店铺其他信息
     *
     * @param categoryIds  分类ids
     * @param brandIds     品牌ids
     * @param brands       自定义品牌集合
     * @param storeId      店铺id
     * @param customerName 用户名
     */
    private void saveOtherStoreInfo(long[] categoryIds, long[] brandIds, List<Brand> brands, long storeId, String customerName) throws Exception {
        //先删后增
        productBaseClient.deleteSignedCategory(storeId);
        productBaseClient.deleteStoreBrand(storeId);
        productBaseClient.batchDeleteCustomBrand(storeId);
        //添加签约分类
        if (!ArrayUtils.isEmpty(categoryIds)) {
            List<com.qiyee.shop.entity.product.category.StoreSignedCategory> storeSignedCategories = new ArrayList<>();
            Arrays.stream(categoryIds).forEach(categoryId -> storeSignedCategories.add(new StoreSignedCategory().getStoreSignedCategory(storeId, categoryId)));
            productBaseClient.addSignedCategory(storeSignedCategories);
        }
        //添加店铺品牌
        if (!ArrayUtils.isEmpty(brandIds)) {
            List<BrandApply> brandApplies = new ArrayList<>();
            Arrays.stream(brandIds).forEach(brandId -> brandApplies.add(new BrandApply().getBrandApply(storeId, brandId)));
            productBaseClient.addStoreBrand(brandApplies);
        }
        //添加自定义品牌
        if (!CollectionUtils.isEmpty(brands)) {
            List<Brand> brandList = new ArrayList<>();
            brands.forEach(brand -> brandList.add(new Brand().addMySelfBrand(brand.getName(), brand.getUrl(), brand.getCertificatUrl(), storeId, customerName)));
            productBaseClient.batchAddCustomBrand(brandList);
        }
    }

    @Override
    @Transactional
    public int addStore(StoreBusiness storeBusiness) throws Exception {
        logger.debug("addStore and storeBusiness:{}", storeBusiness);
        Customer customer = customerService.queryCustomerByName(storeBusiness.getMobile());
        if (Objects.nonNull(customer)) {
            if (Objects.nonNull(customer.getStoreId()) && CommonConstant.ADMIN_STOREID != customer.getStoreId()) {
                logger.error("addStore fail:already has store");
                return -2;
            }
            storeInfoMapper.addStore(storeBusiness.getStoreInfo());
            storeBusiness.getStoreInfo().setStatus("2");
            long storeId = storeBusiness.getStoreInfo().getId();
            customer.setStoreId(storeId);
            customer.setType("2");
            customerService.updateStoreIdAndType(customer);
            saveOtherStoreInfo(storeBusiness.getCategoryIds(), storeBusiness.getBrandIds(), storeBusiness.getBrands(), storeId, customer.getUserName());
            //为该用户添加权限
            storeRoleService.linkStaffRole(new RoleAndCustomer().getRoleAndCustomer(customer.getId(), 2));
            //设置店铺审核通过
            int res = storeInfoMapper.passStoreAudit(storeBusiness.getStoreInfo());
            if (res == 1) {
                //初始化商家模板
                storeTemplateService.initTemplate(storeBusiness.getStoreInfo().getId());
            }
            return 1;
        }
        logger.error("addStore fail:customer is null");
        return -1;
    }

    /**
     * 开店查询店铺信息
     *
     * @param customerId 会员id
     * @param status     品牌状态 状态  0 申请中  1通过 2 拒绝
     * @return 店铺信息
     */
    @Override
    public StoreBusinessInfo queryStoreBusinessInfoForOpneStore(long customerId, String status) throws Exception {
        long storeId = customerService.queryCustomerWithNoPasswordById(customerId).getStoreId();
        QueryCustomBrandByStoreIdAndStatusReqDTO queryCustomBrandByStoreIdAndStatusReqDTO = QueryCustomBrandByStoreIdAndStatusReqDTO.builder().storeId(storeId).status(status).build();
        QueryStoreBrandsReqDTO reqDTO = QueryStoreBrandsReqDTO.builder().storeId(storeId).status(status).build();
        return new StoreBusinessInfo().getStoreBusinessInfo(queryStoreInfo(storeId), productClient.queryTwoCategoryByStoreId(storeId),
                productClient.queryThreeCategoryByStoreId(storeId), productClient.queryStoreBrands(reqDTO),
                productClient.queryCustomBrandByStoreIdAndStatus(reqDTO));
    }

    /**
     * 开店-查询店铺信息
     *
     * @param storeId 店铺id
     * @param status  品牌状态 状态  0 申请中  1通过 2 拒绝
     * @return 店铺信息
     */
    @Override
    @Transactional
    public StoreBusinessInfo queryStoreBusinessInfo(long storeId, String status) throws Exception {
        logger.debug("queryStoreBusinessInfo and storeId:{}\r\n status:{}", storeId, status);
        QueryCustomBrandByStoreIdAndStatusReqDTO brandByStoreIdAndStatusReqDTO = QueryCustomBrandByStoreIdAndStatusReqDTO.builder().status(status).storeId(storeId).build();
        QueryStoreBrandsReqDTO reqDTO = QueryStoreBrandsReqDTO.builder().status(status).storeId(storeId).build();
        return new StoreBusinessInfo().getStoreBusinessInfo(queryStoreInfo(storeId), productClient.queryTwoCategoryByStoreId(storeId),
                productClient.queryThreeCategoryByStoreId(storeId), productClient.queryStoreBrands(reqDTO),
                productClient.queryCustomBrandByStoreIdAndStatus(reqDTO));
    }

    /**
     * 查询已审核/未审核商家集合
     *
     * @param pageHelper     分页帮助类
     * @param status         店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭
     * @param companyName    公司名称
     * @param storeName      店铺名称
     * @param createTime     创建时间
     * @param customerMobile 用户手机号
     * @param provinceId     省份id
     * @return 已审核/未审核商家集合
     */
    @Override
    public PageHelper<StoreInfo> queryStoreInfoForAuditList(PageHelper<StoreInfo> pageHelper, String status, String companyName, String storeName, String createTime, String customerMobile, long provinceId) {
        logger.debug("queryStoreInfoForAuditList and status:{}\r\n companyName:{}\r\n storeName:{}\r\n createTime:{}", status, companyName, storeName, createTime);
        Map<String, Object> params = new HashMap<>();
        params.put("status", status);
        params.put("companyName", companyName);
        params.put("storeName", storeName);
        params.put("createTime", createTime);
        if (!StringUtils.isEmpty(customerMobile)) {
            params.put("mobile", customerMobile);
        }
        params.put("provinceId", provinceId);
        pageHelper.setListDates(storeInfoMapper.queryStoreInfoForAuditList(pageHelper.getQueryParams(params, storeInfoMapper.queryStoreInfoForAuditListCount(params))));
        for (StoreInfo storeInfo : pageHelper.getList()) {
            try {
                storeInfo.setCategorys(productClient.queryTwoCategoryByStoreId(storeInfo.getId()));
            } catch (BizException e) {
                e.printStackTrace();
            }
        }
        return pageHelper;
    }

    /**
     * 编辑店铺有效期,结算周期,是否关店
     *
     * @param storeInfo 店铺信息
     * @return 编辑返回码
     */
    @Override
    @Transactional
    public int editStoreTimeAndIsClose(StoreInfo storeInfo) throws Exception {
        logger.debug("editStoreTimeAndIsClose and storeInfo:{}", storeInfo);
        if ("4".equals(storeInfo.getStatus())) {
            //更改商品为下架状态
            UpdateShelvesStatusByStoreIdsReqDTO reqDTO = UpdateShelvesStatusByStoreIdsReqDTO.builder().status("0").storeIds(Arrays.asList(storeInfo.getId())).build();
            productBaseClient.updateShelvesStatusByStoreIds(reqDTO);
        }
        //更改店铺信息
        storeInfoMapper.editStoreTimeAndIsClose(storeInfo);
        //更新es
        esClient.indexAllSpu();
       // consumer.accept(1);
        return 1;
    }

    /**
     * 通过商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @Override
    @Transactional
    public int passStoreAudit(StoreInfo storeInfo) throws Exception {
        logger.debug("passStoreAudit and storeInfo :{}", storeInfo);
        passStoreAuditDealData(storeInfo.getId());
        int res = storeInfoMapper.passStoreAudit(storeInfo);
        if (res == 1) {
            //初始化商家模板
            storeTemplateService.initTemplate(storeInfo.getId());
        }
        return res;
    }

    /**
     * 拒绝商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @Override
    public int refuseStoreAudit(StoreInfo storeInfo) {
        logger.debug("refuseStoreAudit and storeInfo :{}", storeInfo);
        return storeInfoMapper.refuseStoreAudit(storeInfo);
    }

    /**
     * 删除商家
     *
     * @param id 商家id
     * @return 成功返回1，失败返回0
     */
    @Override
    @Transactional
    public int deleteStore(long id) throws Exception {
        logger.debug("deleteStore and id :{}", id);
        deleteStoreDealData(id);
        return storeInfoMapper.deleteStore(id);
    }

    /**
     * 编辑店铺信息-客服QQ-公司信息-银行信息
     *
     * @param storeInfo 店铺信息实体类
     * @param flag      1客服QQ 2公司信息 3银行信息
     * @return -1参数错误编辑失败 1 编辑成功
     */
    @Override
    public int editMyStoreInfo(StoreInfo storeInfo, String flag) {
        logger.debug("editMyStoreInfo and storeInfo :{}\r\n flag:{}", storeInfo, flag);
        if ("1".equals(flag)) {
            return storeInfoMapper.editStoreInfoForServiceQQ(storeInfo);
        }
        if ("2".equals(flag)) {
            return storeInfoMapper.editStoreInfoForCompanyInfo(storeInfo);
        }
        if ("3".equals(flag)) {
            return storeInfoMapper.editStoreInfoForBankInfo(storeInfo);
        }
        return -1;
    }

    @Override
    public int queryStoreState(Long storeId) {
        logger.debug("queryStoreState and storeId:{}", storeId);
        if (ObjectUtils.isEmpty(storeId) || storeId <= 0) {
            logger.error("queryStoreState error : storeId is null ");
            return -1;
        }
        StoreInfo storeInfo = storeInfoMapper.queryStoreInfo(storeId);
        if (ObjectUtils.isEmpty(storeInfo)) {
            logger.error("queryStoreState error : store not exist ");
            return -2;
        }
        if ("1".equals(storeInfo.getDelFlag())) {
            logger.error("queryStoreState error : store is deleted ");
            return -3;
        }
        if ("1".equals(storeInfo.getStatus())) {
            logger.error("queryStoreState error : store is waiting for review ");
            return -4;
        }
        if ("3".equals(storeInfo.getStatus())) {
            logger.error("queryStoreState error : store is been rejected ");
            return -5;
        }
        if ("4".equals(storeInfo.getStatus())) {
            logger.error("queryStoreState error : store is close ");
            return -6;
        }
        if (!"2".equals(storeInfo.getStatus())) {
            logger.error("queryStoreState error : store is not pass ");
            return -4;
        }
        if (storeInfo.getEffectiveTime().isBefore(LocalDateTime.now())) {
            logger.error("queryStoreState error : store is not effective ");
            return -7;
        }
        return 1;
    }

    @Override
    public PageHelper<StoreInfo> queryStoreInfoForSearch(PageHelper<StoreInfo> pageHelper, String keyword, int orderBy) throws Exception {
        logger.debug("queryStoreInfoForSearch and keyword:{} \r\n orderBy:{}", keyword, orderBy);
        Map<String, Object> params = new HashMap<>();
        params.put("keyword", keyword);
        params.put("orderBy", orderBy);
        List<StoreInfo> storeInfos = storeInfoMapper.queryStoreInfoForSearch(pageHelper.getQueryParams(params, storeInfoMapper.queryStoreInfoForSearchCount(params)));
        List<StoreInfo> storeList = Lists.newArrayList();
        if (org.apache.commons.lang3.ObjectUtils.isNotEmpty(storeInfos)) {
            for (StoreInfo storeInfo : storeInfos) {
                StoreInfo store = storeInfo.buildStoreScore(storeCommentService.queryStoreScore(storeInfo.getId()));
                store = store.bulidSkus(productClient.queryFiveDataForAttentionStore(storeInfo.getId()));
                store.buildSkusCount(productClient.querySkuCountByStoreId(storeInfo.getId()));
                City city = storeInfoMapper.queryCityById(storeInfo.getCityId());
                store.setCityName(Optional.ofNullable(city).orElseGet(City::new).getName());
                storeList.add(store);
            }
        }
        return pageHelper.setListDates(storeList);
    }

    @Override
    @Transactional
    public int closeStores(List<Long> ids) throws Exception {
        logger.debug("closeStores and ids:{} ", ids);
        if (CollectionUtils.isEmpty(ids)) {
            logger.error("closeStores fail : ids is empty");
            return 0;
        }
        //更改店铺商品状态为下架
        UpdateShelvesStatusByStoreIdsReqDTO reqDTO = UpdateShelvesStatusByStoreIdsReqDTO.builder().storeIds(ids).status("0").build();
        productBaseClient.updateShelvesStatusByStoreIds(reqDTO);
        //关闭店铺
        storeInfoMapper.closeStores(ids);
        //重新创建索引
        esFeign.indexAllSpu();
        return 1;
    }

    @Override
    public int checkStoreNameExist(String storeName, long storeId) {
        logger.debug("checkStoreNameExist and storeName:{}", storeName);
        Map<String, Object> params = new HashMap<>();
        params.put("storeName", storeName);
        params.put("storeId", storeId);
        return storeInfoMapper.queryStoreCountByStoreName(params);
    }

    /**
     * 校验公司名是否存在(store端开店用)
     *
     * @param companyName 店铺名称
     * @param customerId  用户ID
     * @return 0 可用  1 不可用
     */
    @Override
    public int checkCompanyNameForOpenStore(String companyName, long customerId) {
        logger.debug("checkCompanyNameForOpenStore and companyName:{} , customerId: {}", companyName, customerId);
        Map<String, Object> params = new HashMap<>(2);
        params.put("companyName", companyName);
        params.put("storeId", customerService.queryCustomerWithNoPasswordById(customerId).getStoreId());
        return storeInfoMapper.queryStoreCountByCompanyName(params);
    }

    @Override
    public int checkCompanyNameExist(String companyName, long storeId) {
        logger.debug("checkCompanyNameExist and companyName:{}", companyName);
        Map<String, Object> params = new HashMap<>();
        params.put("companyName", companyName);
        params.put("storeId", storeId);
        return storeInfoMapper.queryStoreCountByCompanyName(params);
    }

    @Override
    public StoreInfo selStoreInfo(Long storeId) {
        logger.debug("selStoreInfo and storeId:{}", storeId);

        if (Objects.isNull(storeId)) {
            logger.error("selStoreInfo fail : storeId is null");
            return null;
        }
        StoreInfo storeInfo = this.queryStoreInfo(storeId);
        //获取店铺评分平均数
        storeInfo.setAveScore(storeEvaluationService.queryStoreAveScore(storeId));
        return storeInfo;
    }

    @Override
    public StoreInfo queryOneOnSaleStore(String skuId, long cityId) {
        logger.debug("queryOneOnSaleStore and skuId:{} \r\n cityId:{}", skuId, cityId);
        StoreInfo storeInfo = storeInfoMapper.queryOneOnSaleStore(skuId, cityId);
        if (Objects.isNull(storeInfo)) {
            logger.error("queryOneOnSaleStore fail : no storeInfo");
            return null;
        }
        return storeInfo.buildAveScore(storeEvaluationService.queryStoreAveScore(storeInfo.getId()));
    }

    @Override
    public List<StoreInfo> queryOnSaleStoreList(String skuId, long cityId) {
        logger.debug("queryOnSaleStoreList and skuId:{} \r\n cityId:{}", skuId, cityId);
        return storeInfoMapper.queryOnSaleStoreList(skuId, cityId);
    }

    @Override
    public StoreInfo queryOneOnSaleStoreByCoordinate(OnSaleStoreQueryParam onSaleStoreQueryParam) {
        logger.debug("queryOneOnSaleStore and OnSaleStoreQueryParam:{}", onSaleStoreQueryParam);
        return storeInfoMapper.queryOneOnSaleStoreByCoordinate(onSaleStoreQueryParam);
    }

    @Override
    public List<StoreInfo> queryOnSaleStoreListByCoordinate(OnSaleStoreQueryParam onSaleStoreQueryParam) {
        logger.debug("queryOnSaleStoreList and OnSaleStoreQueryParam:{}", onSaleStoreQueryParam);
        return storeInfoMapper.queryOnSaleStoreListByCoordinate(onSaleStoreQueryParam);
    }

    @Override
    public List<StoreInfo> queryNearByStoreList(BigDecimal longitude, BigDecimal latitude, int distance) {
        logger.debug("queryNearByStoreList and longitude:{} \r\n latitude:{}", longitude, latitude);
        return storeInfoMapper.queryNearByStoreList(longitude, latitude, distance);
    }

    @Override
    public PageHelper<StoreInfo> queryStoreInfoList(PageHelper<StoreInfo> pageHelper, String companyName, String storeName, String customerMobile) {
        logger.debug("queryStoreInfoList and pageHelper:{} \r\n companyName:{} \r\n storeName:{} \r\n customerMobile:{} \r\n", pageHelper, companyName, storeName, customerMobile);
        Map<String, Object> params = new HashMap<>();
        params.put("companyName", companyName);
        params.put("storeName", storeName);
        if (!StringUtils.isEmpty(customerMobile)) {
            params.put("mobile", customerMobile);
        }
        return pageHelper.setListDates(storeInfoMapper.queryStoreInfoList(pageHelper.getQueryParams(params, storeInfoMapper.queryStoreInfoListCount(params))));

    }

    @Override
    public int openStoreForOutLetStore(long storeId) {
        logger.debug("openStoreForOutLetStore and storeId:{}", storeId);
        return storeInfoMapper.openStoreForOutLetStore(storeId);
    }

    @Override
    public boolean isEffective(long storeId) {
        logger.debug("isEffective and storeId:{}", storeId);
        if (CommonConstant.ADMIN_STOREID == storeId) {
            return true;
        }
        StoreInfo storeInfo = storeInfoMapper.queryStoreInfo(storeId);
        return Objects.nonNull(storeInfo) && storeInfo.isEffective();
    }


    @Override
    public List<StoreInfo> queryNearStoreList(BigDecimal longitude, BigDecimal latitude, int distance) throws Exception {
        logger.debug("queryNearStoreList and longitude:{} \r\n latitude:{}", longitude, latitude);

        List<StoreInfo> storeInfos = storeInfoMapper.queryNearStoreList(longitude, latitude, distance);
        List<StoreInfo> result = Lists.newArrayList();
        if (org.apache.commons.lang3.ObjectUtils.isNotEmpty(storeInfos)) {
            for (StoreInfo storeInfo : storeInfos) {
                StoreInfo store = storeInfo.buildStoreScore(storeCommentService.queryStoreScore(storeInfo.getId()));
                store = store.bulidSkus(productClient.queryFiveDataForAttentionStore(storeInfo.getId()));
                store = storeInfo.buildSkusCount(productClient.querySkuCountByStoreId(storeInfo.getId()));
                result.add(store);
            }
        }
        return result;
    }


    @Override
    public StoreReview queryStoreReview(Customer customer) {
        logger.debug("queryStoreReview and customer :{}", customer);

        if (Objects.isNull(customer)) {
            logger.error("queryStoreReview fail due to customer is null....");
            return StoreReview.buildFail("", "");
        }

        // 根据会员的店铺id查询店铺信息
        StoreInfo storeInfo = this.queryStoreInfo(customerService.queryCustomerWithNoPasswordById(customer.getId()).getStoreId());

        logger.info("queryStoreReview and  and storeInfo:{}", storeInfo);

        //如果没有店铺信息，或者开店状态为0，则返回资料未提交完成
        if (Objects.isNull(storeInfo) || "0".equals(storeInfo.getStatus()) || 0 == storeInfo.getId()) {
            logger.info("queryStoreReview :storeInfo is null or status is 0");
            return StoreReview.buildUnFinishData(customer.getUserName());
        }

        //开店状态为2，表示通过审核
        if ("2".equals(storeInfo.getStatus())) {
            logger.info("queryStoreReview : and revire succes....");
            return StoreReview.buildSuccess(customer.getUserName());
        }
        //审核中
        if ("1".equals(storeInfo.getStatus())) {
            logger.info("queryStoreReview and under revirew....");
            return StoreReview.buildUnderReview(customer.getUserName());
        }

        //审核失败
        if ("3".equals(storeInfo.getStatus())) {
            logger.error("queryStoreReview and review fail...");
            return StoreReview.buildFail(customer.getUserName(), storeInfo.getReason());
        }

        return StoreReview.buildFail("", "");
    }

    @Override
    public PCStoreReview queryPCStoreReview(Long customerId) {
        logger.debug("queryPCStoreReview and customerId :{}", customerId);
        Customer customer = customerService.queryCustomerWithNoPasswordById(customerId);
        if(customer != null){
            // 根据会员的店铺id查询店铺信息
            StoreInfo storeInfo = this.queryStoreInfo(customer.getStoreId());
            logger.info("queryStoreReview and  and storeInfo:{}", storeInfo);
			// 未提交申请
			if (storeInfo == null) {
				return PCStoreReview.buildNotApplied();
			}
            //开店状态为2，表示通过审核
            if ("2".equals(storeInfo.getStatus())) {
                logger.info("queryStoreReview : and revire succes....");
                return PCStoreReview.buildSuccess(customer.getUserName(), storeInfo.getStoreName());
            }
            //审核中
            if ("1".equals(storeInfo.getStatus())) {
                logger.info("queryStoreReview and under revirew....");
                return PCStoreReview.buildUnderReview(customer.getUserName(), storeInfo.getId(), storeInfo.getStoreName());
            }
            //审核失败
            if ("3".equals(storeInfo.getStatus())) {
                logger.error("queryStoreReview and review fail...");
                return PCStoreReview.buildFail(customer.getUserName(), storeInfo.getStoreName(), storeInfo.getId(), storeInfo.getReason());
            }
        }
        return null;
    }

    @Override
    public PageHelper<PCStoreListInfo> queryPCStoreInfoForAuditList(PageHelper<PCStoreListInfo> pageHelper, String status, String companyName,
                                                              String storeName, String customerMobile, String customerName) {
        logger.debug("queryStoreInfoForAuditList and status:{}\r\n companyName:{}\r\n storeName:{}\r\n customerName:{}", status, companyName, storeName, customerName);
        Map<String, Object> params = new HashMap<>();
        params.put("status", status);
        params.put("companyName", companyName);
        params.put("storeName", storeName);
        params.put("customerName", customerName);
        if (!StringUtils.isEmpty(customerMobile)) {
            params.put("mobile", customerMobile);
        }
        List<PCStoreListInfo> list = storeInfoMapper.queryPCStoreInfoForAuditList(pageHelper.getQueryParams(params, storeInfoMapper.queryPCStoreInfoForAuditListCount(params)));
        list.forEach(pcStoreListInfo -> {
            try {
                pcStoreListInfo.setFirstCategory(productClient.queryFirstCategoryByStoreId(pcStoreListInfo.getId()));
            } catch (BizException e) {
                e.printStackTrace();
            }
        });
        return pageHelper.setListDates(list);
    }

    /**
     * 店铺审核通过后处理相关数据
     *
     * @param storeId 店铺id
     */
    @Transactional
    protected void passStoreAuditDealData(long storeId) throws Exception {
        //为该用户添加权限
        storeRoleService.linkStaffRole(new RoleAndCustomer().getRoleAndCustomer(customerService.queryCustomerIdByStoreId(storeId).getId(), 3));
        //将该店铺下的自定义品牌变为审核通过
        productBaseClient.passCustomBrandByStoreId(storeId);
        //将店铺的签约品牌变为审核通过
        productBaseClient.passBrandAuditByStoreId(storeId);
    }

    /**
     * 删除店铺后处理数据
     *
     * @param storeId 店铺id
     */
    @Transactional
    protected void deleteStoreDealData(long storeId) throws Exception {
        //删除签约分类
        productBaseClient.deleteSignedCategory(storeId);
        //删除店铺品牌
        productBaseClient.deleteStoreBrand(storeId);
        //删除自定义品牌
        productBaseClient.batchDeleteCustomBrand(storeId);
    }

	@Override
	public CommonResponse<String> addAnchorSell(List<AnchorSell> anchorSellList) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("anchorList", anchorSellList);
		int result = anchorSellMapper.batchInsert(map);
		log.info("addAnchorSell anchorSellList: {}, result : {}", JSONObject.toJSONString(anchorSellList), result);
		if (result > 0) {
			return CommonResponse.build("新增成功");
		}
		return CommonResponse.build("请稍后再试", CommonConstant.NO_USE_PANIC);
	}

    @Override
    public CommonResponse<String> deleteAnchorSell(AnchorSell anchorSell) {
        try {
            log.debug("deleteAnchorSell anchorSell : {} " + JSONObject.toJSONString(anchorSell));
            anchorSellMapper.delete(anchorSell.getCustomerId(), anchorSell.getStoreId(), anchorSell.getSellType());
            return CommonResponse.build("取消关注成功");
        } catch (Exception e) {
            log.error("开播前删除当前用户下面的未开售的商品失败", e);
            return CommonResponse.build("请稍后再试", CommonConstant.NO_USE_PANIC);
        }
    }

    @Override
    public CommonResponse<AnchorSellVo> queryAnchorList(AnchorSell anchorSell) {
        AnchorSellVo anchorSellVo = new AnchorSellVo();
        log.info("queryAnchorList anchorSell : {}", JSONObject.toJSONString(anchorSell));
        try {
            List<AnchorSell> anchorSellList = anchorSellMapper
                    .queryCountGroupBySellType(anchorSell.getStoreId(), anchorSell.getCustomerId(),
                            anchorSell.getSellType(), anchorSell.getSpuName(), anchorSell.getRoomId());
            if (CollectionUtils.isEmpty(anchorSellList)) {
                anchorSellVo.setSellReadyCount(0);
                anchorSellVo.setSellOnCount(0);
                anchorSellVo.setAnchorList(new ArrayList<>());
                return CommonResponse.build(anchorSellVo);
            }
            Map<Integer, Integer> nameMap = anchorSellList.stream()
                    .collect(Collectors.toMap(AnchorSell::getSellType, AnchorSell::getCount));
            anchorSellVo.setSellReadyCount(nameMap.get(0) == null ? 0 : nameMap.get(0));
            anchorSellVo.setSellOnCount(nameMap.get(1) == null ? 0 : nameMap.get(1));
            anchorSellVo.setAnchorList(anchorSellMapper
                    .queryAnchorList(anchorSell.getStoreId(), anchorSell.getCustomerId(), anchorSell.getSellType(),
                            anchorSell.getSpuName(), anchorSell.getRoomId()));
            log.info("queryAnchorList anchorSellVo : {}", JSONObject.toJSONString(anchorSellVo));
            return CommonResponse.build(anchorSellVo);
        } catch (Exception e) {
            log.error("查询当前主播未开播 已开播的商品列表失败", e);
            return CommonResponse.build(null, CommonConstant.NO_USE_PANIC);
        }
    }

    @Override
    public CommonResponse<String> updateAnchorSell(AnchorSell anchorSell) {
        try {
            AnchorSell anchorSell1 = anchorSellMapper.queryAnchorSell(anchorSell.getId());
            updateSpuStatus(anchorSell1.getRoomId(), anchorSell1.getSkuId().toString(),
                    anchorSell.getSellType().toString());
            anchorSellMapper.update(anchorSell);
            return CommonResponse.build("更新成功");
        } catch (Exception e) {
            log.error("更新失败！" + e.getMessage());
            return CommonResponse.build("更新失败", CommonConstant.NO_USE_PANIC);
        }
    }

    /**
     * 更新商品售卖状态
     */
    public void updateSpuStatus(Long roomId, String goodsId, String goodsState) throws ServiceException {
        Map<String, String> map = new HashMap<>();
//        try {
//            map.put("room_id", String.valueOf(roomId));
//            map.put("goods_id", goodsId);
//            map.put("goods_state", goodsState);
//            String sign = MD5Utils.MD5Encode(goodsId + roomId + goodsState + token, "utf-8");
//            map.put("token", sign);
//            logger.debug("updateSpuStatus=" + JSONObject.toJSONString(map));
//            String result = HttpUtil.requestOfPost(goodUrl, map);
//            if (StringUtils.isEmpty(result)) {
//                throw new ServiceException("-1", "updateSpuStatus请求结果为空");
//            }
//            JSONObject jsonObject = JSONObject.parseObject(result);
//            String status = jsonObject.getString("status");
//            if (CommonConstant.COMMUNITYBUYHEAD_WAIT_AUDIT.equals(status)) {
//                throw new ServiceException("-1", jsonObject.getString("error"));
//            }
//            logger.debug("==============" + jsonObject.toString());
//            jsonObject.getString("list");
//        } catch (Exception e) {
//            logger.error("更新商品售卖状态接口报错", e);
//            throw new ServiceException("-1", e.getMessage());
//        }
    }

	@Override
	public PageHelper<StoreInfo> queryStoreInfoForLiveSearch(PageHelper<StoreInfo> pageHelper, String storeName) {
		logger.info("queryStoreInfoForLiveSearch and pageHelper:{} \r\n storeName:{}", pageHelper, storeName);
		Map<String, Object> params = new HashMap<>(1);
		params.put("storeName", storeName);
		int totalCount = storeInfoMapper.queryStoreInfoForLiveSearchCount(params);
		List<StoreInfo> storeInfos = new ArrayList<>();
		if (StringUtils.isEmpty(storeName) || "平台自营".contains(storeName)) {
			// 如果带有自营的名称搜索，则总数+1，算上自营
			totalCount += 1;
			if (pageHelper.getPageNum() == 0) {
				// 如果是带有自营的名称搜索, 并且是第一页的话, 返回自营的店铺数据
				storeInfos.add(0, StoreInfo.newPlatformStoreInfo());
			}
		}
		storeInfos.addAll(storeInfoMapper.queryStoreInfoForLiveSearch(pageHelper.getQueryParams(params, totalCount)));
		return pageHelper.setListDates(storeInfos);
	}
}
