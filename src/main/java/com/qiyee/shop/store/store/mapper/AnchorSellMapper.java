package com.qiyee.shop.store.store.mapper;

import com.qiyee.shop.entity.openstore.AnchorSell;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * 主播开售mapper
 *
 * @author sunluyang on 2017/6/13.
 */
@Repository
public interface AnchorSellMapper {
	/**
	 * 新增主播开售记录
	 *
	 * @param map
	 */
	int batchInsert(Map<String, Object> map);

	/**
	 * 开播前删除当前用户下面的未开售的商品
	 * @param customerId 会员id
	 * @param storeId 店铺id
	 * @param sellType 开售商品类型 0:未开售 1：已开售
	 */
	void delete(@Param("storeId") Long storeId, @Param("customerId") Long customerId,
			@Param("sellType") Integer sellType);

	/**
	 * 查询主播销售记录
	 * @param storeId 门店id
	 * @param customerId 会员id
	 * @param sellType  开售商品类型 0:未开售 1：已开售
	 * @param spuName 商品名称
	 * @param roomId 直播间id
	 * @return 返回开售记录
	 */
	List<AnchorSell> queryCountGroupBySellType(@Param("storeId") Long storeId, @Param("customerId") Long customerId,
			@Param("sellType") Integer sellType, @Param("spuName") String spuName, @Param("roomId") Long roomId);

	/**
	 * 查询主播销售记录
	 * @param storeId 门店id
	 * @param customerId 会员id
	 * @param sellType  开售商品类型 0:未开售 1：已开售
	 * @param spuName 商品名称
	 * @param roomId 直播间id
	 * @return 返回开售记录
	 */
	List<AnchorSell> queryAnchorList(@Param("storeId") Long storeId, @Param("customerId") Long customerId,
			@Param("sellType") Integer sellType, @Param("spuName") String spuName, @Param("roomId") Long roomId);

	/**
	 * 根据id查询开售记录
	 * @param id 记录id
	 * @return 返回开售记录实体
	 */
	AnchorSell queryAnchorSell(long id);

	void update(AnchorSell anchorSell);
}
