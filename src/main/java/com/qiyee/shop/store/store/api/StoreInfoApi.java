package com.qiyee.shop.store.store.api;

import com.qiyee.shop.dto.store.LiveStoreInfoDTO;
import com.qiyee.shop.dto.store.StoreInfoConsumerDto;
import com.qiyee.shop.dto.store.StoreInfoDto;
import com.qiyee.shop.entity.customer.customer.Customer;
import com.qiyee.shop.entity.customer.openstore.*;
import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.entity.openstore.AnchorSell;
import com.qiyee.shop.entity.openstore.AnchorSellVo;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.store.store.service.StoreInfoService;
import com.qiyee.shop.util.CommonResponse;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.*;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 店铺信息
 */
@RequestMapping("/store/info")
@RestController
@Api("店铺信息接口")
public class StoreInfoApi {

    @Autowired
    private StoreInfoService storeInfoService;

    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeId 店铺id
     * @return 店铺信息
     */
    @GetMapping("/queryStoreInfo")
    @ApiOperation(value = "根据店铺id查询店铺信息", notes = "根据店铺id查询店铺信息")
    public StoreInfo queryStoreInfo(@RequestParam(value = "storeId") Long storeId){
        return storeInfoService.queryStoreInfo(storeId);
    }

	/**
	 * 根据店铺id查询店铺信息
	 *
	 * @param storeId 店铺id
	 * @return 店铺信息
	 */
	@GetMapping("/queryStoreNameByStoreId")
	@ApiOperation(value = "根据店铺id查询店铺信息", notes = "根据店铺id查询店铺信息")
	public Map<String, Object> queryStoreNameByStoreId(@RequestParam(value = "storeId") Long storeId){
		return storeInfoService.queryStoreNameByStoreId(storeId);
	}

    /**
     * 根据店铺id查询店铺信息
     *
     * @param storeName 店铺id
     * @return 店铺信息
     */
    @GetMapping("/listStoreByStoreName")
    @ApiOperation(value = "根据店铺名称查询店铺信息", notes = "根据店铺名称查询店铺信息")
    public List<StoreInfo> listStoreByStoreName(@RequestParam(value = "storeName") String storeName){
        return storeInfoService.listStoreByStoreName(storeName);
    }
    /**
     * 根据店铺id列表查询店铺信息
     *
     * @param storeIds 店铺id
     * @return 店铺信息
     */
    @PostMapping("/listStoreByStoreIds")
    @ApiOperation(value = "根据店铺ID列表查询店铺信息", notes = "根据店铺ID列表查询店铺信息")
    public List<StoreInfo> listStoreByStoreIds(@RequestBody List<Long> storeIds){
        if(ObjectUtils.isNotEmpty(storeIds)){
            return storeInfoService.listStoreByStoreIds(storeIds);
        }
        return null;
    }

    /**
     * 开店处理店铺信息 (PC端)
     *
     * @param storeInfo  店铺实体类
     * @param customerId 会员ID
     * @return 返回值跳转页面
     */
    @PutMapping("/dealPCStoreInfo/{customerId}")
    @ApiOperation(value = "开店处理店铺信息 (PC端)", notes = "开店处理店铺信息 (PC端)（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "开店处理店铺信息 (PC端)", response = Coupon.class)
    })
    public PCStoreReview dealPCStoreInfo(@RequestBody StoreInfo storeInfo,@PathVariable(value = "customerId") long customerId){
        return storeInfoService.dealPCStoreInfo(storeInfo,customerId);
    }


    /**
     * 开店处理店铺信息
     *
     * @param storeInfo  店铺实体类
     * @param customerId 会员ID
     * @return 返回值跳转页面
     */
    @PutMapping("/dealStoreInfo/{customerId}")
    @ApiOperation(value = "开店处理店铺信息", notes = "开店处理店铺信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员ID"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "开店处理店铺信息", response = Coupon.class)
    })
    public int dealStoreInfo(@RequestBody StoreInfo storeInfo,@PathVariable(value = "customerId") long customerId){
        return storeInfoService.dealStoreInfo(storeInfo,customerId);
    }


    /**
     * 查询开店时填写的信息
     *
     * @param customerId
     * @return 店铺信息
     */
    @GetMapping("/findOpenStoreInfo/{customerId}")
    @ApiOperation(value = "查询开店时填写的信息", notes = "查询开店时填写的信息（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询开店时填写的信息", response = Coupon.class)
    })
    public StoreInfo findOpenStoreInfo(@PathVariable(value = "customerId") long customerId){
        return storeInfoService.findOpenStoreInfo(customerId);
    }

    /**
     * 处理店铺经营类型
     *
     * @param customerId  会员Id
     * @param storeName   店铺名称
     * @param categoryIds 分类ids
     * @param brandIds    品牌ids
     * @param brands      自定义品牌集合
     * @return 添加返回码
     */
    @PutMapping("/dealStoreBusinessInfo/{customerId}")
    @ApiOperation(value = "处理店铺经营类型", notes = "处理店铺经营类型（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员ID"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "categoryIds", value = "分类ids"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "brandIds", value = "品牌ids"),
            @ApiImplicitParam(paramType = "body", dataType = "List<Brand>", name = "brands", value = "自定义品牌集合"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "处理店铺经营类型", response = Coupon.class)
    })
    public int dealStoreBusinessInfo(@PathVariable(value = "customerId") long customerId,
                                     @RequestParam(value = "storeName") String storeName,
                                     @RequestParam(value = "categoryIds") long[] categoryIds,
                                     @RequestParam(value = "brandIds") long[] brandIds,
                                     @RequestBody(required = false) List<Brand> brands) throws Exception{
        return storeInfoService.dealStoreBusinessInfo(customerId,storeName,categoryIds,brandIds,brands);
    }

    /**
     * 新增店铺（admin端新增）
     *
     * @param storeBusiness 店铺信息实体
     * @return -1用户不存在 1成功 -2 该用户下已有店铺
     */
    @PostMapping("/addStore")
    @ApiOperation(value = "新增店铺（admin端新增）", notes = "新增店铺（admin端新增）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员ID"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "categoryIds", value = "分类ids"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "brandIds", value = "品牌ids"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "新增店铺（admin端新增）", response = Coupon.class)
    })
    public int addStore(@RequestBody StoreBusiness storeBusiness) throws Exception{
        return storeInfoService.addStore(storeBusiness);
    }

    /**
     * 开店查询店铺信息
     *
     * @param storeId 店铺id
     * @param status  品牌状态 状态  0 申请中  1通过 2 拒绝
     * @return 店铺信息
     */
    @GetMapping("/queryStoreBusinessInfo/{storeId}")
    @ApiOperation(value = "开店查询店铺信息", notes = "开店查询店铺信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "status", value = "品牌状态 状态  0 申请中  1通过 2 拒绝"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "开店查询店铺信息", response = Coupon.class)
    })
    public StoreBusinessInfo queryStoreBusinessInfo(@PathVariable(value = "storeId") long storeId,
                                                    @RequestParam(value = "status",required = false) String status) throws Exception{
        return storeInfoService.queryStoreBusinessInfo(storeId,status);
    }

    /**
     * 开店查询店铺信息
     *
     * @param customerId 会员id
     * @param status     品牌状态 状态  0 申请中  1通过 2 拒绝
     * @return 店铺信息
     */
    @GetMapping("/queryStoreBusinessInfoForOpneStore/{customerId}")
    @ApiOperation(value = "开店查询店铺信息", notes = "开店查询店铺信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "status", value = "品牌状态 状态  0 申请中  1通过 2 拒绝"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "开店查询店铺信息", response = Coupon.class)
    })
    public StoreBusinessInfo queryStoreBusinessInfoForOpneStore(@PathVariable(value = "customerId") long customerId,
                                                                @RequestParam(value = "status") String status) throws Exception{
        return storeInfoService.queryStoreBusinessInfoForOpneStore(customerId,status);
    }

    /**
     * 查询已审核/未审核商家集合
     *
     * @param pageHelper     分页帮助类
     * @param status         店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭
     * @param companyName    公司名称
     * @param storeName      店铺名称
     * @param createTime     创建时间
     * @param customerMobile 用户手机号
     * @param provinceId     省份id
     * @return 已审核/未审核商家集合
     */
    @PostMapping("/queryStoreInfoForAuditList/{provinceId}")
    @ApiOperation(value = "查询已审核/未审核商家集合", notes = "分页查查询已审核/未审核商家集合询门店订单（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "companyName", value = "公司名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "createTime", value = "创建时间"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "customerMobile", value = "用户手机号"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "provinceId", value = "省份id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询已审核/未审核商家集合", response = Coupon.class)
    })
    public PageHelper<StoreInfo> queryStoreInfoForAuditList(@RequestBody PageHelper<StoreInfo> pageHelper,
                                                            @RequestParam(value = "status",required = false) String status,
                                                            @RequestParam(value = "companyName",required = false) String companyName,
                                                            @RequestParam(value = "storeName",required = false) String storeName,
                                                            @RequestParam(value = "createTime",required = false) String createTime,
                                                            @RequestParam(value = "customerMobile",required = false) String customerMobile,
                                                            @PathVariable("provinceId") long provinceId){
        return storeInfoService.queryStoreInfoForAuditList(pageHelper,status,companyName,storeName,createTime,customerMobile,provinceId);
    }



    /**
     * 招商入驻-查询已审核/未审核商家集合
     *
     * @param pageHelper     分页帮助类
     * @param status         店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭
     * @param companyName    公司名称
     * @param storeName      店铺名称
     * @param customerMobile 用户手机号
     * @return 已审核/未审核商家集合
     */
    @PostMapping("/queryPCStoreInfoForAuditList")
    @ApiOperation(value = "招商入驻-查询已审核/未审核商家集合", notes = "招商入驻-分页查查询已审核/未审核商家集合询门店订单（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "status", value = "店铺状态 0填写资料中 1店铺审核中 2审核通过 3审核不通过 4店铺关闭"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "companyName", value = "公司名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "customerMobile", value = "会员手机号"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "customerName", value = "会员名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询已审核/未审核商家集合", response = Coupon.class)
    })
    public PageHelper<PCStoreListInfo> queryPCStoreInfoForAuditList(@RequestBody PageHelper<PCStoreListInfo> pageHelper,
                                                            @RequestParam(value = "status",required = false) String status,
                                                            @RequestParam(value = "companyName",required = false) String companyName,
                                                            @RequestParam(value = "storeName",required = false) String storeName,
                                                            @RequestParam(value = "customerMobile",required = false) String customerMobile,
                                                              @RequestParam(value = "customerName",required = false) String customerName){
        return storeInfoService.queryPCStoreInfoForAuditList(pageHelper,status,companyName,storeName,customerMobile,customerName);
    }

//    @PostMapping("/queryPCStoredetail/{storeId}")
//    @ApiOperation(value = "根据店铺id查询店铺信息", notes = "根据店铺id查询店铺信息")
//    public QueryPCStoredetail queryPCStoredetail(@RequestParam(value = "storeId") Long storeId){
//
//    }

    /**
     * 编辑店铺有效期,结算周期,是否关店
     *
     * @return 编辑返回码
     */
    @PostMapping("/editStoreTimeAndIsClose")
    @ApiOperation(value = "编辑店铺有效期,结算周期,是否关店", notes = "编辑店铺有效期,结算周期,是否关店（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "编辑店铺有效期,结算周期,是否关店", response = Coupon.class)
    })
    public int editStoreTimeAndIsClose(@RequestBody StoreInfoConsumerDto storeInfoConsumerDto) throws Exception{
        return storeInfoService.editStoreTimeAndIsClose(storeInfoConsumerDto.getStoreInfo());
    }

    /**
     * 通过商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/passStoreAudit")
    @ApiOperation(value = "通过商家审核", notes = "通过商家审核（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "通过商家审核", response = Coupon.class)
    })
    public int passStoreAudit(@RequestBody StoreInfo storeInfo)throws Exception{
        return storeInfoService.passStoreAudit(storeInfo);
    }

    /**
     * 拒绝商家审核
     *
     * @param storeInfo 商家实例
     * @return 成功返回1，失败返回0
     */
    @PutMapping("/refuseStoreAudit")
    @ApiOperation(value = "拒绝商家审核", notes = "拒绝商家审核（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "拒绝商家审核", response = Coupon.class)
    })
    public int refuseStoreAudit(@RequestBody StoreInfo storeInfo){
        return storeInfoService.refuseStoreAudit(storeInfo);
    }

    /**
     * 删除商家
     *
     * @param id 商家id
     * @return 成功返回1，失败返回0
     */
    @DeleteMapping("/deleteStore/{id}")
    @ApiOperation(value = "删除商家", notes = "删除商家（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "商家id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "删除商家", response = Coupon.class)
    })
    public int deleteStore(@PathVariable(value = "id") long id)throws Exception{
        return storeInfoService.deleteStore(id);
    }

    /**
     * 编辑店铺信息-客服QQ-公司信息-银行信息
     *
     * @param storeInfo 店铺信息实体类
     * @param flag      1客服QQ 2公司信息 3银行信息
     * @return -1参数错误编辑失败 1 编辑成功
     */
    @PutMapping("/editMyStoreInfo")
    @ApiOperation(value = "编辑店铺信息-客服QQ-公司信息-银行信息", notes = "编辑店铺信息-客服QQ-公司信息-银行信息（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "flag", value = "1客服QQ 2公司信息 3银行信息"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "编辑店铺信息-客服QQ-公司信息-银行信息", response = Coupon.class)
    })
    public int editMyStoreInfo(@RequestBody StoreInfo storeInfo,
                               @RequestParam(value = "flag") String flag){
        return storeInfoService.editMyStoreInfo(storeInfo,flag);
    }

    /**
     * 获取商铺状态
     * @param storeId 店铺id
     * @return 返回码 1：正常营业  -1：storeId为空，参数不全  -2:store不存在 -3 店铺被删除
     * -4 店铺审核中 -5 审核不通过 -6 店铺关闭 -7 超过有效期
     */
    @GetMapping("/queryStoreState")
    @ApiOperation(value = "获取商铺状态", notes = "获取商铺状态（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "Long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取商铺状态", response = Coupon.class)
    })
    public int queryStoreState(@RequestParam(value = "storeId") Long storeId){
        return storeInfoService.queryStoreState(storeId);
    }


    /**
     * 获取商铺状态_PC端
     * @param customerId 店铺id
     * @return 返回码 1：正常营业  -1：storeId为空，参数不全  -2:store不存在 -3 店铺被删除
     * -4 店铺审核中 -5 审核不通过 -6 店铺关闭 -7 超过有效期
     */
    @GetMapping("/queryPCStoreState")
    @ApiOperation(value = "获取商铺状态_PC端", notes = "获取商铺状态_PC端（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "Long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "获取商铺状态", response = Coupon.class)
    })
    public PCStoreReview queryPCStoreState(@RequestParam(value = "customerId") Long customerId){
        return storeInfoService.queryPCStoreReview(customerId);
    }

    /**
     * 根据条件搜索店铺
     *
     * @return 店铺集合
     */
    @PostMapping("/queryStoreInfoForSearch")
    @ApiOperation(value = "根据条件搜索店铺", notes = "根据条件搜索店铺（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "keyword", value = "关键字"),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "orderBy", value = "排序条件 0:综合 1:销量 2:评论数"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据条件搜索店铺", response = Coupon.class)
    })
    public PageHelper<StoreInfo> queryStoreInfoForSearch(@RequestBody StoreInfoDto storeInfoDto) throws Exception{
        return storeInfoService.queryStoreInfoForSearch(storeInfoDto.getPageHelper(),storeInfoDto.getKeyword(),storeInfoDto.getOrderBy());
    }

    /**
     * 批量关店
     *
     * @param ids      店铺id
     * @return 成功返回>0 失败返回0
     */
    @PutMapping("/closeStores")
    @ApiOperation(value = "批量关店", notes = "批量关店（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "批量关店", response = Coupon.class)
    })
    public int closeStores(@RequestParam("ids") List<Long> ids) throws Exception{
        return storeInfoService.closeStores(ids);
    }

    /**
     * 校验店铺名是否存在
     *
     * @param storeName 店铺名
     * @param storeId 店铺id
     * @return >0存在 否则不存在
     */
    @GetMapping("/checkStoreNameExist/{storeId}")
    @ApiOperation(value = "校验店铺名是否存在", notes = "校验店铺名是否存在（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "storeName", value = "店铺名"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "校验店铺名是否存在", response = Coupon.class)
    })
    public int checkStoreNameExist(@RequestParam(value = "storeName") String storeName,
                                   @PathVariable(value = "storeId") long storeId){
        return storeInfoService.checkStoreNameExist(storeName,storeId);
    }

    /**
     * 校验公司名是否存在
     *
     * @param companyName 公司名
     * @return >0存在 否则不存在
     */
    @GetMapping("/checkCompanyNameExist/{storeId}")
    @ApiOperation(value = "校验公司名是否存在", notes = "校验公司名是否存在（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "companyName", value = "公司名"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "校验公司名是否存在", response = Coupon.class)
    })
    public int checkCompanyNameExist(@RequestParam(value = "companyName") String companyName,
                                     @PathVariable(value = "storeId") long storeId){
        return storeInfoService.checkCompanyNameExist(companyName,storeId);
    }


    /**
     * 校验公司名是否存在(store端开店用)
     *
     * @param companyName 店铺名称
     * @param customerId  用户ID
     * @return 0 可用  1 不可用
     */
    @GetMapping("/checkCompanyNameForOpenStore/{customerId}")
    @ApiOperation(value = "校验公司名是否存在(store端开店用)", notes = "校验公司名是否存在(store端开店用)（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "companyName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "校验公司名是否存在(store端开店用)", response = Coupon.class)
    })
    public int checkCompanyNameForOpenStore(@RequestParam(value = "companyName") String companyName,
                                            @PathVariable(value = "customerId") long customerId){
        return storeInfoService.checkCompanyNameForOpenStore(companyName,customerId);
    }


    /**
     * 根据店铺id查询店铺信息(门店评分)
     *
     * @param storeId 商家ID
     * @return 店铺信息
     */
    @GetMapping("/selStoreInfo")
    @ApiOperation(value = "根据店铺id查询店铺信息(门店评分)", notes = "根据店铺id查询店铺信息(门店评分)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据店铺id查询店铺信息(门店评分)", response = StoreInfo.class)
    })
    public StoreInfo selStoreInfo(@RequestParam(value = "storeId") Long storeId){
        return storeInfoService.selStoreInfo(storeId);
    }

    /**
     * 查找一个在售门店
     *
     * @param skuId  单品id
     * @param cityId 市id
     * @return 门店信息
     */
    @GetMapping("/queryOneOnSaleStore/{cityId}")
    @ApiOperation(value = "查找一个在售门店", notes = "查找一个在售门店")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "skuId", value = "单品id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "cityId", value = "市id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查找一个在售门店", response = Coupon.class)
    })
    public StoreInfo queryOneOnSaleStore(@RequestParam(value = "skuId") String skuId,
                                         @PathVariable(value = "cityId") long cityId){
        return storeInfoService.queryOneOnSaleStore(skuId,cityId);
    }

    /**
     * 查找在售门店
     *
     * @param skuId  单品id
     * @param cityId 市id
     * @return 门店信息集合
     */
    @GetMapping("/queryOnSaleStoreList/{cityId}")
    @ApiOperation(value = "查找一个在售门店", notes = "查找一个在售门店")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "String", name = "skuId", value = "单品id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "cityId", value = "市id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查找一个在售门店", response = Coupon.class)
    })
    public List<StoreInfo> queryOnSaleStoreList(@RequestParam(value = "skuId") String skuId,
                                                @PathVariable(value = "cityId") long cityId){
        return storeInfoService.queryOnSaleStoreList(skuId,cityId);
    }


    /**
     * 查找一个在售门店(根据经纬度距离排序)
     *
     * @param onSaleStoreQueryParam 在售门店搜索参数
     * @return 门店信息
     */
    @PostMapping("/queryOneOnSaleStoreByCoordinate")
    @ApiOperation(value = "查找一个在售门店(根据经纬度距离排序)", notes = "查找一个在售门店(根据经纬度距离排序)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查找一个在售门店(根据经纬度距离排序)", response = Coupon.class)
    })
    public StoreInfo queryOneOnSaleStoreByCoordinate(@RequestBody OnSaleStoreQueryParam onSaleStoreQueryParam){
        return storeInfoService.queryOneOnSaleStoreByCoordinate(onSaleStoreQueryParam);
    }

    /**
     * 查找在售门店(根据经纬度距离排序)
     *
     * @param onSaleStoreQueryParam 在售门店搜索参数
     * @return 门店信息集合
     */
    @PostMapping("/queryOnSaleStoreListByCoordinate")
    @ApiOperation(value = "查找在售门店(根据经纬度距离排序)", notes = "查找在售门店(根据经纬度距离排序)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查找在售门店(根据经纬度距离排序)", response = Coupon.class)
    })
    public List<StoreInfo> queryOnSaleStoreListByCoordinate(@RequestBody OnSaleStoreQueryParam onSaleStoreQueryParam){
        return storeInfoService.queryOnSaleStoreListByCoordinate(onSaleStoreQueryParam);
    }


    /**
     * 根据定位查询附近门店列表
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  范围（km）
     * @return 门店信息
     */
    @GetMapping("/queryNearByStoreList/{distance}")
    @ApiOperation(value = "根据定位查询附近门店列表", notes = "根据定位查询附近门店列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "BigDecimal", name = "longitude", value = "经度"),
            @ApiImplicitParam(paramType = "path", dataType = "BigDecimal", name = "latitude", value = "纬度"),
            @ApiImplicitParam(paramType = "path", dataType = "int", name = "distance", value = "范围（km）"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据定位查询附近门店列表", response = Coupon.class)
    })
    public List<StoreInfo> queryNearByStoreList(@RequestParam(value = "longitude") BigDecimal longitude,
                                                @RequestParam(value = "latitude") BigDecimal latitude,
                                                @PathVariable(value = "distance") int distance){
        return storeInfoService.queryNearByStoreList(longitude,latitude,distance);
    }

    /**
     * 搜索门店列表
     *
     * @param pageHelper     分页帮助类
     * @param companyName    公司名称
     * @param storeName      店铺名称
     * @param customerMobile 手机号码
     * @return 返回门店列表
     */
    @PostMapping("/queryStoreInfoList")
    @ApiOperation(value = "根据定位查询附近门店列表", notes = "根据定位查询附近门店列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "companyName", value = "公司名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "storeName", value = "店铺名称"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "customerMobile", value = "手机号码"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据定位查询附近门店列表", response = Coupon.class)
    })
    public PageHelper<StoreInfo> queryStoreInfoList(@RequestBody PageHelper<StoreInfo> pageHelper,
                                                    @RequestParam("companyName") String companyName,
                                                    @RequestParam("storeName") String storeName,
                                                    @RequestParam("customerMobile") String customerMobile){
        return storeInfoService.queryStoreInfoList(pageHelper,companyName,storeName,customerMobile);
    }

    /**
     * 开启店铺（门店用）
     *
     * @param storeId 店铺id
     * @return 1成功 否则失败
     */
    @PutMapping("/openStoreForOutLetStore/{storeId}")
    @ApiOperation(value = "开启店铺（门店用）", notes = "开启店铺（门店用）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "开启店铺（门店用）", response = Coupon.class)
    })
    public int openStoreForOutLetStore(@PathVariable("storeId") long storeId){
        return storeInfoService.openStoreForOutLetStore(storeId);
    }


    /**
     * 判断店铺是否有效
     *
     * @param storeId 店铺id
     * @return 有效返回true, 否则返回false
     */
    @GetMapping("/isEffective/{storeId}")
    @ApiOperation(value = "判断店铺是否有效", notes = "判断店铺是否有效")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "判断店铺是否有效", response = Coupon.class)
    })
    public boolean isEffective(@PathVariable(value = "storeId") long storeId){
        return storeInfoService.isEffective(storeId);
    }

    /**
     * 根据定位查询附近店铺列表
     *
     * @param longitude 经度
     * @param latitude  纬度
     * @param distance  范围（km）
     * @return 店铺信息
     */
    @GetMapping("/queryNearStoreList/{distance}")
    @ApiOperation(value = "根据定位查询附近店铺列表", notes = "根据定位查询附近店铺列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "BigDecimal", name = "longitude", value = "经度"),
            @ApiImplicitParam(paramType = "path", dataType = "BigDecimal", name = "latitude", value = "纬度"),
            @ApiImplicitParam(paramType = "path", dataType = "int", name = "distance", value = "范围（km）"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据定位查询附近店铺列表", response = Coupon.class)
    })
    public List<StoreInfo> queryNearStoreList(@RequestParam("longitude") BigDecimal longitude,
                                              @RequestParam("latitude") BigDecimal latitude,
                                              @PathVariable("distance") int distance) throws Exception{
        return storeInfoService.queryNearStoreList(longitude,latitude,distance);
    }


    /**
     * 查看店铺的审核状态
     *
     * @param customer 当前登录的会员信息
     * @return 返回店铺的审核状态
     */
    @PostMapping("/queryStoreReview")
    @ApiOperation(value = "查看店铺的审核状态", notes = "查看店铺的审核状态")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查看店铺的审核状态", response = Coupon.class)
    })
    public StoreReview queryStoreReview(@RequestBody Customer customer){
        return storeInfoService.queryStoreReview(customer);
    }

	/**
	 * 添加商品到未开售的列表里面
	 * @param anchorSellList 开售记录
	 */
	@PostMapping("/addSpuReadySell")
	@ApiOperation(value = "添加商品到未开售的列表里面", notes = "添加商品到未开售的列表里面")
	public CommonResponse<String> addSpuReadySell(@RequestBody List<AnchorSell> anchorSellList) {
		return storeInfoService.addAnchorSell(anchorSellList);
	}

	/**
	 * 开播前删除当前用户下面的未开售的商品
	 *
	 * @param anchorSell 开售记录
	 */
	@PostMapping("/deleteSpuReadySell")
	@ApiOperation(value = "开播前删除当前用户下面的未开售的商品", notes = "开播前删除当前用户下面的未开售的商品")
	public CommonResponse<String> deleteSpuReadySell(@RequestBody AnchorSell anchorSell) {
		return storeInfoService.deleteAnchorSell(anchorSell);
	}

	/**
	 * 查询当前主播未开播 已开播的商品列表
	 *
	 * @param anchorSell 开售记录
	 */
	@PostMapping("/queryAnchorSellList")
	@ApiOperation(value = "查询当前主播未开播 已开播的商品列表", notes = "查询当前主播未开播 已开播的商品列表")
	public CommonResponse<AnchorSellVo> queryAnchorSellList(@RequestBody AnchorSell anchorSell) {
		return storeInfoService.queryAnchorList(anchorSell);
	}

	/**
	 * 更新主播销售接口
	 *
	 * @param anchorSell
	 */
	@PostMapping("/updateAnchorSell")
	@ApiOperation(value = "updateAnchorSell", notes = "更新主播销售接口")
	public CommonResponse<String> updateAnchorSell(@RequestBody AnchorSell anchorSell) {
		return storeInfoService.updateAnchorSell(anchorSell);
	}

	/**
	 * 直播时分页搜索店铺
	 *
	 * @return 店铺集合
	 */
	@PostMapping("/queryStoreInfoForLiveSearch")
	@ApiOperation(value = "直播时分页搜索店铺", notes = "直播时分页搜索店铺（不需要认证）", httpMethod = "POST")
	@ApiImplicitParams({@ApiImplicitParam(paramType = "query", dataType = "int", name = "pageNum", value = "当前页"),
			@ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
			@ApiImplicitParam(paramType = "query", dataType = "string", name = "storeName", value = "店铺名称")})
	@ApiResponses({@ApiResponse(code = 200, message = "店铺信息", response = StoreInfo.class)})
	public PageHelper<StoreInfo> queryStoreInfoForLiveSearch(@RequestBody LiveStoreInfoDTO liveStoreInfoDTO) {
		return storeInfoService
				.queryStoreInfoForLiveSearch(liveStoreInfoDTO.getPageHelper(), liveStoreInfoDTO.getStoreName());
	}
}
