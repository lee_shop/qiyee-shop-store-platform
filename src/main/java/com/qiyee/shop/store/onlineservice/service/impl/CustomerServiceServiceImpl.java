package com.qiyee.shop.store.onlineservice.service.impl;

import com.qiyee.shop.entity.site.onlineservice.CustomerService;
import com.qiyee.shop.entity.site.onlineservice.CustomerServiceInfo;
import com.qiyee.shop.store.onlineservice.mapper.CustomerServiceMapper;
import com.qiyee.shop.store.onlineservice.service.CustomerServiceService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;

/**
 * 在线客服service实现层
 */
@Service
@RequiredArgsConstructor
public class CustomerServiceServiceImpl implements CustomerServiceService {

    /**
     * 注入在线客服实现类
     */
    private final CustomerServiceMapper customerServiceMapper;

    /**
     * 调试日志
     */
    private Logger logger = LoggerFactory.getLogger(CustomerServiceServiceImpl.class);

    /**
     * 查询在线客服
     *
     * @return 在线客服实体类
     */
    @Override
    public CustomerService queryCustomerService() {
        //查询在线客服设置
        CustomerService customerService = customerServiceMapper.queryCustomerService();
        //查询在线客服信息集合
        List<CustomerServiceInfo> customerServiceInfos = customerServiceMapper.queryCustomerServiceInfo();
        if (Objects.nonNull(customerService) && !CollectionUtils.isEmpty(customerServiceInfos)) {
            logger.debug("queryCustomerService  customerServiceInfos is empty...");
            customerService.setCustomerServiceInfo(customerServiceInfos);
        }
        return customerService;
    }

    /**
     * 编辑在线客服,先删后增
     *
     * @param customerService 在线客服实体类
     * @return 返回删除结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int editCustomerServiceInfo(CustomerService customerService) {
        if (Objects.isNull(customerService)) {
            logger.error("editCustomerServiceInfo error due to customerService is null");
            return -1;
        }
        customerServiceMapper.deleteCustomerServiceInfo();
        if (!CollectionUtils.isEmpty(customerService.getCustomerServiceInfo())) {
            logger.debug("editCustomerServiceInfo CustomerServiceInfo isNotEmpty");
            customerServiceMapper.addCustomerServiceInfo(customerService.getCustomerServiceInfo());
        }
        if (0 == customerService.getId()) {
            logger.info("editCustomerServiceInfo : add");
            return customerServiceMapper.addCustomerService(customerService);
        } else {
            logger.info("editCustomerServiceInfo : update");
            return customerServiceMapper.editCustomerService(customerService);
        }
    }
}
