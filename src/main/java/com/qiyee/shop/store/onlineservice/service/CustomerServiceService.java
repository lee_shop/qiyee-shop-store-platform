package com.qiyee.shop.store.onlineservice.service;


import com.qiyee.shop.entity.site.onlineservice.CustomerService;

/**
 * 在线客服service接口层
 */
public interface CustomerServiceService {
    /**
     * 查询在线客服
     *
     * @return 在线客服实体类
     */
    CustomerService queryCustomerService();

    /**
     * 编辑在线客服
     *
     * @param customerService 在线客服实体类
     * @return 返回编辑行数
     */
    int editCustomerServiceInfo(CustomerService customerService);
}
