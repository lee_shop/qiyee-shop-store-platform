package com.qiyee.shop.store.onlineservice.api;

import com.qiyee.shop.store.feign.product.Category.dto.SpuCategory;
import com.qiyee.shop.entity.site.onlineservice.CustomerService;
import com.qiyee.shop.store.onlineservice.service.CustomerServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 在线客服
 */
@RequestMapping("/store/customerservice")
@RestController
@Api("在线客服服务接口")
public class CustomerServiceApi {

    @Autowired
    private CustomerServiceService customerServiceService;

    /**
     * 查询在线客服
     *
     * @return 在线客服实体类
     */
    @GetMapping("/queryCustomerService")
    @ApiOperation(value = "查询在线客服", notes = "查询在线客服(需要认证)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询在线客服", response = SpuCategory.class)
    })
    public CustomerService queryCustomerService(){
        return customerServiceService.queryCustomerService();
    }

    /**
     * 编辑在线客服
     *
     * @param customerService 在线客服实体类
     * @return 返回编辑行数
     */
    @PostMapping("/editCustomerServiceInfo")
    @ApiOperation(value = "编辑在线客服", notes = "编辑在线客服(需要认证)")
    @ApiResponses({
            @ApiResponse(code = 200, message = "编辑在线客服", response = SpuCategory.class)
    })
    public int editCustomerServiceInfo(@RequestBody CustomerService customerService){
        return customerServiceService.editCustomerServiceInfo(customerService);
    }
}
