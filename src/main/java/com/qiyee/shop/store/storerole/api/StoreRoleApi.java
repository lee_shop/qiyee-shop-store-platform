package com.qiyee.shop.store.storerole.api;


import com.qiyee.shop.entity.system.sotremenu.StoreMenu;
import com.qiyee.shop.entity.system.storerole.RoleAndCustomer;
import com.qiyee.shop.entity.system.storerole.StoreRole;
import com.qiyee.shop.store.storerole.service.StoreRoleService;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/store/role")
@Api(tags = "web端角色接口")
public class StoreRoleApi {


    /**
     * 店铺角色service
     */
    @Autowired
    private StoreRoleService storeRoleService;

    /**
     * 查询所有角色
     *
     * @param roleName 空查询所有,不为空按条件查询
     * @param isPaging 是否分页 1需要 0不需要
     * @return 角色集合
     */
    @ApiOperation(value = "查询所有角色", notes = "查询所有角色")
    @PostMapping("/queryAllStoreRole/{isPaging}/{storeId}")
    PageHelper<StoreRole> queryAllStoreRole(@RequestBody PageHelper<StoreRole> pageHelper,
                                            @RequestParam("roleName") String roleName,
                                            @PathVariable("isPaging") int isPaging,
                                            @PathVariable("storeId") long storeId){
        return storeRoleService.queryAllStoreRole(pageHelper, roleName, isPaging, storeId);
    }

    /**
     * 根据用户Id查询角色菜单
     *
     * @param customerId 会员Id
     * @return 菜单
     */
    @ApiOperation(value = "根据用户Id查询角色菜单", notes = "根据用户Id查询角色菜单")
    @GetMapping("/storeRoleAuthMenu/{customerId}")
    List<StoreMenu> storeRoleAuthMenu(@PathVariable("customerId") long customerId){
        return storeRoleService.storeRoleAuthMenu(customerId);
    }

    /**
     * 添加角色并关联角色权限
     *
     * @param roleName 角色实体类
     * @param authIds  权限Id
     * @param storeId  店铺id
     * @return 返回添加结果-1用户名存在 -2没有权限id >=1添加成功
     */
    @ApiOperation(value = "添加角色并关联角色权限", notes = "添加角色并关联角色权限")
    @PostMapping("/addStoreRole/{storeId}")
    int addStoreRole(@RequestParam("roleName") String roleName,
                     @RequestParam("authIds") long[] authIds,
                     @PathVariable("storeId") long storeId){
        return storeRoleService.addStoreRole(roleName, authIds, storeId);
    }


    /**
     * 根据角色ID查询该角色拥有对权限ID
     *
     * @param roleId 角色ID
     * @return 权限ID集合
     */
    @ApiOperation(value = "根据角色ID查询该角色拥有对权限ID", notes = "根据角色ID查询该角色拥有对权限ID")
    @GetMapping("/queryAuthIdByRoleId/{roleId}")
    List<Long> queryAuthIdByRoleId(@PathVariable("roleId") long roleId){
        return storeRoleService.queryAuthIdByRoleId(roleId);
    }


    /**
     * 删除角色
     *
     * @param roleId 角色id
     * @return 删除返回值
     */
    @ApiOperation(value = "删除角色", notes = "删除角色")
    @DeleteMapping("/deleteRole/{storeId}")
    int deleteRole(@RequestParam("roleId") long[] roleId,
                   @PathVariable("storeId") long storeId){
        return storeRoleService.deleteRole(roleId, storeId);
    }

    /**
     * 编辑角色
     *
     * @param roleId   角色id
     * @param authIds  权限id
     * @param roleName 角色名称
     * @param storeId  店铺id
     * @return 编辑结果 -1用户名存在 -2没有权限id >=1编辑成功
     */
    @ApiOperation(value = "编辑角色", notes = "编辑角色")
    @PutMapping("/editRole/{roleId}/{storeId}")
    int editRole(@PathVariable("roleId") long roleId,
                 @RequestParam("roleName") String roleName,
                 @RequestParam("authIds") long[] authIds,
                 @PathVariable("storeId") long storeId){
        return storeRoleService.editRole(roleId, roleName, authIds, storeId);
    }

    /**
     * 查询店铺角色用于添加员工
     *
     * @param storeId 店铺id
     * @return 店铺角色集合
     */
    @ApiOperation(value = "查询店铺角色用于添加员工", notes = "查询店铺角色用于添加员工")
    @GetMapping("/queryStoreRoleForAddStaff/{storeId}")
    List<StoreRole> queryStoreRoleForAddStaff(@PathVariable("storeId") long storeId){
        return storeRoleService.queryStoreRoleForAddStaff(storeId);
    }

    /**
     * 添加员工进行添加角色和会员关联表
     *
     * @param roleAndCustomer 实体类
     * @return 添加返回码
     */
    @ApiOperation(value = "添加员工进行添加角色和会员关联表", notes = "添加员工进行添加角色和会员关联表")
    @PostMapping("/linkStaffRole")
    int linkStaffRole(@RequestBody RoleAndCustomer roleAndCustomer){
        return storeRoleService.linkStaffRole(roleAndCustomer);
    }

    /**
     * 批量删除员工的角色关联数据
     *
     * @param customerIds 员工id
     * @return 删除返回码
     */
    @ApiOperation(value = "批量删除员工的角色关联数据", notes = "批量删除员工的角色关联数据")
    @DeleteMapping("/deleteStoreStaff")
    int deleteStoreStaff(@RequestParam("customerIds") List<Long> customerIds){
        return storeRoleService.deleteStoreStaff(customerIds);
    }

    /**
     * 编辑员工-编辑员工关联的角色id
     *
     * @param roleAndCustomer 实体类
     * @return 编辑返回码
     */
    @ApiOperation(value = "编辑员工-编辑员工关联的角色id", notes = "编辑员工-编辑员工关联的角色id")
    @PutMapping("/updateRoleId")
    int updateRoleId(@RequestBody RoleAndCustomer roleAndCustomer){
        return storeRoleService.updateRoleId(roleAndCustomer);
    }

    /**
     * 查询会员的店铺角色
     *
     * @param customerId 会员id
     * @return 返回会员的店铺角色
     */
    @ApiOperation(value = "查询会员的店铺角色", notes = "查询会员的店铺角色")
    @GetMapping("/queryCustomerStoreRole/{customerId}")
    StoreRole queryCustomerStoreRole(@PathVariable("customerId") long customerId){
        return storeRoleService.queryCustomerStoreRole(customerId);
    }
}
