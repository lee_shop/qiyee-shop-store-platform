package com.qiyee.shop.store.reservation.service;


import com.qiyee.shop.entity.order.reservation.Reservation;
import com.qiyee.shop.util.PageHelper;

import java.util.List;

/**
 * Created by administrator on 18/4/10
 * 门店预约服务接口
 */
public interface ReservationService {

    /**
     * 新增门店预约
     *
     * @param reservation 预约实体
     * @return 成功1 失败0
     */
    int addReservation(Reservation reservation);

    /**
     * 查询门店预约列表
     *
     * @param pageHelper 分页帮助类
     * @param customerId 会员id
     * @param skuName    单品名称
     * @return 门店预约列表
     */
    PageHelper<Reservation> queryReservationList(PageHelper<Reservation> pageHelper, long customerId, String skuName);

    /**
     * 取消门店预约
     *
     * @param ids        门店预约id数组
     * @param customerId 会员id
     * @param storeId    门店id
     * @return 成功>0 失败0
     */
    int cancelReservation(Long[] ids, long customerId, long storeId);

    /**
     * 查询门店预约列表（门店用，不带分页）
     *
     * @param customerId 会员id
     * @param storeId    店铺id
     * @return 门店预约集合
     */
    List<Reservation> queryReservationListForStore(long customerId, long storeId);

    /**
     * 通过id查询门店预约列表
     *
     * @param ids     预约id数组
     * @param storeId 门店id
     * @return 门店预约集合
     */
    List<Reservation> queryReservationListForStoreByIds(Long[] ids, long storeId);


    /**
     * 修改预约商品数量
     *
     * @param num     预约商品数量
     * @param id      预约id
     * @param storeId 门店id
     * @return 成功1 失败0
     */
    int updateReservationNum(int num, long id, long storeId);

}
