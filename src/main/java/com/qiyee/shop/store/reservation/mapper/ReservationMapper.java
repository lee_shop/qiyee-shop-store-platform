package com.qiyee.shop.store.reservation.mapper;


import com.qiyee.shop.entity.order.reservation.Reservation;

import java.util.List;
import java.util.Map;

/**
 * Created by administrator on 18/4/10
 * 门店预约数据库接口
 */
public interface ReservationMapper {

    /**
     * 新增门店预约
     *
     * @param reservation 预约实体
     * @return 成功1 失败0
     */
    int addReservation(Reservation reservation);

    /**
     * 查询门店预约列表
     *
     * @param params 查询参数
     * @return 门店预约集合
     */
    List<Reservation> queryReservationList(Map<String, Object> params);

    /**
     * 查询门店预约总记录数
     *
     * @param params 查询参数
     * @return 门店预约总记录数
     */
    int queryReservationListCount(Map<String, Object> params);

    /**
     * 取消门店预约
     *
     * @param params 参数
     * @return 成功>0 失败0
     */
    int cancelReservation(Map<String, Object> params);

    /**
     * 查询门店预约列表（门店用，不带分页）
     *
     * @param params 查询参数
     * @return 门店预约集合
     */
    List<Reservation> queryReservationListForStore(Map<String, Object> params);

    /**
     * 通过id查询门店预约列表
     *
     * @param params 查询参数
     * @return 门店预约集合
     */
    List<Reservation> queryReservationListForStoreByIds(Map<String, Object> params);

    /**
     * 修改预约商品数量
     *
     * @param params 修改参数
     * @return 成功1 失败0
     */
    int updateReservationNum(Map<String, Object> params);

}
