package com.qiyee.shop.store.reservation.service.impl;

import com.qiyee.shop.entity.order.reservation.Reservation;
import com.qiyee.shop.store.reservation.mapper.ReservationMapper;
import com.qiyee.shop.store.reservation.service.ReservationService;
import com.qiyee.shop.util.PageHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by administrator on 18/4/10
 * 门店预约服务接口实现类
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ReservationServiceImpl implements ReservationService {

    /**
     * 注入门店预约数据库接口
     */
    private final ReservationMapper reservationMapper;

    /**
     * 新增门店预约
     *
     * @param reservation 预约实体
     * @return 成功1 失败0
     */
    @Override
    public int addReservation(Reservation reservation) {
        log.debug("addReservation and reservation", reservation);

        if (Objects.isNull(reservation)) {
            log.error("addReservation fail due to reservation is empty");
            return 0;
        }
        return reservationMapper.addReservation(reservation);
    }

    /**
     * 查询门店预约列表
     *
     * @param pageHelper 分页帮助类
     * @param customerId 会员id
     * @param skuName    单品名称
     * @return 门店预约列表
     */
    @Override
    public PageHelper<Reservation> queryReservationList(PageHelper<Reservation> pageHelper, long customerId, String skuName) {
        log.debug("queryReservationList and pageHelper :{} \r\n customerId :{} \r\n skuName :{}", pageHelper, customerId, skuName);
        Map<String, Object> params = new HashMap<>(2);
        params.put("customerId", customerId);
        params.put("skuName", skuName);
        return pageHelper.setListDates(reservationMapper.queryReservationList(pageHelper.getQueryParams(params, reservationMapper.queryReservationListCount(params))));
    }

    /**
     * 取消门店预约
     *
     * @param ids        门店预约id数组
     * @param customerId 会员id
     * @param storeId    门店id
     * @return 成功>0 失败0
     */
    @Override
    public int cancelReservation(Long[] ids, long customerId, long storeId) {
        log.debug("cancelReservation and ids :{} \r\n customerId :{} \r\n storeId :{}", ids, customerId, storeId);
        if (ArrayUtils.isEmpty(ids)) {
            log.error("cancelReservation fail due to ids is empty");
            return 0;
        }
        Map<String, Object> params = new HashMap<>(3);
        params.put("ids", ids);
        params.put("customerId", customerId);
        params.put("storeId", storeId);
        return reservationMapper.cancelReservation(params);
    }

    /**
     * 查询门店预约列表（门店用，不带分页）
     *
     * @param customerId 会员id
     * @param storeId    门店id
     * @return 门店预约集合
     */
    @Override
    public List<Reservation> queryReservationListForStore(long customerId, long storeId) {
        log.debug("queryReservationListForStore and csutomerId :{} \r\n storeId :{}", customerId, storeId);
        Map<String, Object> params = new HashMap<>(2);
        params.put("customerId", customerId);
        params.put("storeId", storeId);
        return reservationMapper.queryReservationListForStore(params);
    }

    /**
     * 通过id查询门店预约列表
     *
     * @param ids     预约id数组
     * @param storeId 门店id
     * @return 门店预约集合
     */
    @Override
    public List<Reservation> queryReservationListForStoreByIds(Long[] ids, long storeId) {
        log.debug("queryReservationListForStoreByIds and ids :{} \r\n storeId :{}", ids, storeId);
        if (ArrayUtils.isEmpty(ids)) {
            log.error("queryReservationListForStoreByIds fail due to ids is empty");
            return Collections.emptyList();
        }
        Map<String, Object> params = new HashMap<>(2);
        params.put("ids", ids);
        params.put("storeId", storeId);
        return reservationMapper.queryReservationListForStoreByIds(params);
    }


    /**
     * 修改预约商品数量
     *
     * @param num     预约商品数量
     * @param id      预约id
     * @param storeId 门店id
     * @return 成功1 失败0
     */
    @Override
    public int updateReservationNum(int num, long id, long storeId) {
        log.debug("updateReservationNum and num :{} \r\n id :{} \r\n storeId :{}", num, id, storeId);
        Map<String, Object> params = new HashMap<>(3);
        params.put("num", num);
        params.put("id", id);
        params.put("storeId", storeId);
        return reservationMapper.updateReservationNum(params);
    }
}
