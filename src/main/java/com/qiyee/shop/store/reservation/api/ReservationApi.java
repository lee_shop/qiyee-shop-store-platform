package com.qiyee.shop.store.reservation.api;

import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.entity.order.reservation.Reservation;
import com.qiyee.shop.store.reservation.service.ReservationService;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/**
 * Created by administrator on 18/4/10
 * 门店预约服务接口
 */
@RequestMapping("/store/reservation")
@RestController
@Api("门店预约服务接口")
public class ReservationApi {

    @Autowired
    private ReservationService reservationService;

    /**
     * 新增门店预约
     *
     * @param reservation 预约实体
     * @return 成功1 失败0
     */
    @PostMapping("/addReservation")
    @ApiOperation(value = "新增门店预约", notes = "新增门店预约（需要认证）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "新增门店预约", response = Coupon.class)
    })
    public int addReservation(@RequestBody Reservation reservation){
        return reservationService.addReservation(reservation);
    }

    /**
     * 查询门店预约列表
     *
     * @param pageHelper 分页帮助类
     * @param customerId 会员id
     * @param skuName    单品名称
     * @return 门店预约列表
     */
    @PostMapping("/queryReservationList/{customerId}")
    @ApiOperation(value = "查询门店预约列表", notes = "查询门店预约列表（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "form", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "form", dataType = "String", name = "skuName", value = "单品名称"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店预约列表", response = Coupon.class)
    })
    public PageHelper<Reservation> queryReservationList(@RequestBody PageHelper<Reservation> pageHelper,
                                                        @PathVariable("customerId") long customerId,
                                                        @RequestParam("skuName") String skuName){
        return reservationService.queryReservationList(pageHelper,customerId,skuName);
    }

    /**
     * 取消门店预约
     *
     * @param ids        门店预约id数组
     * @param customerId 会员id
     * @param storeId    门店id
     * @return 成功>0 失败0
     */
    @PutMapping("/cancelReservation/{customerId}/{storeId}")
    @ApiOperation(value = "取消门店预约", notes = "取消门店预约（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "ids", value = "门店预约id数组"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "门店id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "取消门店预约", response = Coupon.class)
    })
    public int cancelReservation(@RequestParam(value = "ids") Long[] ids,
                                 @PathVariable(value = "customerId") long customerId,
                                 @PathVariable(value = "storeId") long storeId){
        return reservationService.cancelReservation(ids,customerId,storeId);
    }

    /**
     * 查询门店预约列表（门店用，不带分页）
     *
     * @param customerId 会员id
     * @param storeId    店铺id
     * @return 门店预约集合
     */
    @GetMapping("/queryReservationListForStore/{customerId}/{storeId}")
    @ApiOperation(value = "查询门店预约列表（门店用，不带分页）", notes = "查询门店预约列表（门店用，不带分页）（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "门店id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询门店预约列表（门店用，不带分页）", response = Coupon.class)
    })
    public List<Reservation> queryReservationListForStore(@PathVariable(value = "customerId") long customerId,
                                                          @PathVariable(value = "storeId") long storeId){
        return reservationService.queryReservationListForStore(customerId,storeId);
    }

    /**
     * 通过id查询门店预约列表
     *
     * @param ids     预约id数组
     * @param storeId 门店id
     * @return 门店预约集合
     */
    @GetMapping("/queryReservationListForStoreByIds/{storeId}")
    @ApiOperation(value = "通过id查询门店预约列表", notes = "通过id查询门店预约列表（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "ids", value = "预约id数组"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "门店id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "通过id查询门店预约列表", response = Coupon.class)
    })
    public List<Reservation> queryReservationListForStoreByIds(@RequestParam(value = "ids") Long[] ids,
                                                               @PathVariable(value = "storeId") long storeId){
        return reservationService.queryReservationListForStoreByIds(ids,storeId);
    }


    /**
     * 修改预约商品数量
     *
     * @param num     预约商品数量
     * @param id      预约id
     * @param storeId 门店id
     * @return 成功1 失败0
     */
    @PutMapping("/updateReservationNum/{num}/{id}/{storeId}")
    @ApiOperation(value = "修改预约商品数量", notes = "修改预约商品数量（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "int", name = "num", value = "预约商品数量"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "id", value = "预约id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "门店id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "修改预约商品数量", response = Coupon.class)
    })
    public int updateReservationNum(@PathVariable(value = "num") int num,
                                    @PathVariable(value = "id") long id,
                                    @PathVariable(value = "storeId") long storeId){
        return reservationService.updateReservationNum(num,id,storeId);
    }
}
