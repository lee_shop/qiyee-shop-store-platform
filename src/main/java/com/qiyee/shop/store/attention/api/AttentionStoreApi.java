package com.qiyee.shop.store.attention.api;

import com.qiyee.shop.entity.customer.attention.AttentionStore;
import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.store.attention.service.AttentionStoreService;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 关注店铺
 *
 * Created by administrator on 2019/10/15.
 * 优惠卷接口
 */
@RequestMapping("/store/attention")
@RestController
@Api("关注商户接口")
public class AttentionStoreApi {

    @Autowired
    private AttentionStoreService attentionStoreService;


    /**
     * 根据会员id查询关注的店铺
     *
     * @param pageHelper
     * @param customerId
     * @return 关注的店铺信息集合
     */
    @PostMapping("/queryAttentionByCustomerId/{customerId}")
    @ApiOperation(value = "根据会员id查询关注的店铺", notes = "根据会员id查询关注的店铺（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageNum", value = "当前页"),
            @ApiImplicitParam(paramType = "form", dataType = "int", name = "pageSize", value = "每页显示的记录数"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据会员id查询关注的店铺", response = Coupon.class)
    })
    public PageHelper<AttentionStore> queryAttentionByCustomerId(@RequestBody PageHelper<AttentionStore> pageHelper, @PathVariable("customerId") long customerId){
        return attentionStoreService.queryAttentionByCustomerId(pageHelper,customerId);
    }

    /**
     * 根据店铺id和会员id取消关注
     *
     * @param storeId    店铺id
     * @param customerId 会员id
     * @return 删除返回码
     */
    @PutMapping("/cancelStoreAttention/{storeId}/{customerId}")
    @ApiOperation(value = "根据店铺id和会员id取消关注", notes = "根据店铺id和会员id取消关注（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据店铺id和会员id取消关注", response = Coupon.class)
    })
    public int cancelStoreAttention(@PathVariable(value = "storeId") long storeId,
                                    @PathVariable(value = "customerId")  long customerId){
        return attentionStoreService.cancelStoreAttention(storeId,customerId);
    }

    /**
     * 店铺被关注的数量
     *
     * @param storeId 店铺id
     * @return 返回关注的数量
     */
    @GetMapping("/queryNumByStore/{storeId}")
    @ApiOperation(value = "店铺被关注的数量", notes = "店铺被关注的数量（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "店铺被关注的数量", response = Coupon.class)
    })
    public int queryNumByStore(@PathVariable(value = "storeId") long storeId){
        return attentionStoreService.queryNumByStore(storeId);
    }

    /**
     * 查询会员关注店铺的数量
     *
     * @param customerId 会员id
     * @return 返回会员关注店铺的数量
     */
    @GetMapping("/queryCustomerAttentionStoreCount/{customerId}")
    @ApiOperation(value = "查询会员关注店铺的数量", notes = "查询会员关注店铺的数量（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询会员关注店铺的数量", response = Coupon.class)
    })
    public int queryCustomerAttentionStoreCount(@PathVariable(value = "customerId") long customerId){
        return attentionStoreService.queryCustomerAttentionStoreCount(customerId);
    }

    /**
     * 关注店铺
     *
     * @param customerId 用户id
     * @param storeId    店铺id
     * @return -1 已经关注过 1 关注成功 0 失败
     */
    @GetMapping("/attentionStore/{customerId}/{storeId}")
    @ApiOperation(value = "关注店铺", notes = "关注店铺（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "关注店铺", response = Coupon.class)
    })
    public int attentionStore(@PathVariable(value = "customerId") long customerId,
                              @PathVariable(value = "storeId") long storeId){
        return attentionStoreService.attentionStore(customerId,storeId);
    }

    /**
     * 判断用户是否关注此店铺
     * @param storeId  店铺id
     * @param customerId 用户id
     * @return
     */
    @GetMapping(value = "/existsAttentionStore")
    @ApiOperation(value = "判断用户是否关注店铺", notes = "关注店铺（需要认证）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "storeId", value = "店铺id"),
    })
    public int existsAttentionStore(@RequestParam("storeId") long storeId,@RequestParam("customerId") long customerId){
        return attentionStoreService.existsAttentionStore(storeId,customerId);
    }
}
