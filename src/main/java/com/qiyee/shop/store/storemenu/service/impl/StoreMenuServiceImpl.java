package com.qiyee.shop.store.storemenu.service.impl;

import com.qiyee.shop.entity.system.sotremenu.StoreMenu;
import com.qiyee.shop.entity.system.sotremenu.StoreMenuVo;
import com.qiyee.shop.store.storemenu.mapper.StoreMenuMapper;
import com.qiyee.shop.store.storemenu.service.StoreMenuService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author administrator on 2019/9/23.
 */
@Service
@RequiredArgsConstructor
public class StoreMenuServiceImpl implements StoreMenuService {

    /**
     * 菜单mapper
     */
    private final StoreMenuMapper storeMenuMapper;

    /**
     * 调试日志
     */
    Logger logger = LoggerFactory.getLogger(StoreMenuServiceImpl.class);


    /**
     * 根据管理员id查询菜单
     *
     * @param customerId     管理员id
     * @param isAdminAddShop 是否为admin端添加店铺  true 是  false 否
     * @return 管理员菜单实体类 递归返回一级
     */
    @Override
    public List<StoreMenuVo> queryStoreMenu(Long customerId, boolean isAdminAddShop) {
        logger.debug("queryStoreMenu... and customerId{}, and isAdminShop : {}", customerId, isAdminAddShop);
        List<StoreMenuVo> storeMenuVos = storeMenuMapper.queryMenuVo(customerId);
        // admin端添加门店  修改菜单
        if (isAdminAddShop) {
            return buildChildren(filterMenuName(storeMenuVos));
        }
        return buildChildren(storeMenuVos);
    }


    @Override
    public List<StoreMenu> queryManagerMenus(Long mangerId) {
        //查询所有该用户菜单
        List<StoreMenu> adminMenu = storeMenuMapper.queryStoreMenu(mangerId);
        //返回该用户一级菜单
        List<StoreMenu> parentList = adminMenu.stream().filter(parentMenu -> parentMenu.getParentId() == 0).collect(Collectors.toList());
        //返回该用户二级菜单
        for (StoreMenu parentMenu : parentList) {
            List<StoreMenu> list = new ArrayList();
            for (StoreMenu allMenu : adminMenu) {
                if (parentMenu.getAuthorityId() == allMenu.getParentId() && allMenu.getGrade() == 2) {
                    // 添加的员工角色中去除员工管理
                    if (allMenu.getAuthorityId() == 403) {
                        continue;
                    }
                    list.add(allMenu);
                    parentMenu.setChildMenu(list);
                }
            }
        }
        //返回该用户三级菜单
        for (StoreMenu parentMenu : parentList) {
            if (CollectionUtils.isEmpty(parentMenu.getChildMenu())) {
                continue;
            }
            for (StoreMenu childMenu : parentMenu.getChildMenu()) {
                List<StoreMenu> list = new ArrayList();
                for (StoreMenu allMenu : adminMenu) {
                    if (childMenu.getAuthorityId() == allMenu.getParentId() && allMenu.getGrade() == 3) {
                        list.add(allMenu);
                        childMenu.setChildMenu(list);
                    }
                }
            }
        }
        //返回该用户四级菜单
        for (StoreMenu parentMenu : parentList) {
            if (CollectionUtils.isEmpty(parentMenu.getChildMenu())) {
                continue;
            }
            for (StoreMenu childMenu : parentMenu.getChildMenu()) {
                if (CollectionUtils.isEmpty(childMenu.getChildMenu())) {
                    continue;
                }
                for (StoreMenu thirdMenu : childMenu.getChildMenu()) {
                    List<StoreMenu> list = new ArrayList();
                    for (StoreMenu allMenu : adminMenu) {
                        if (thirdMenu.getAuthorityId() == allMenu.getParentId() && allMenu.getGrade() == 4) {
                            list.add(allMenu);
                            thirdMenu.setChildMenu(list);
                        }
                    }
                }
            }
        }
        return parentList;
    }


    @Override
    public List<String> queryCustomerPermissions(Long customerId) {
        logger.debug("queryCustomerPermissions and customerId:{}", customerId);
        return storeMenuMapper.queryCustomerPermissions(customerId);
    }


    /**
     * 根据店铺类型 过滤菜单名称
     *
     * @param menuVos 菜单
     * @return 过滤后菜单
     */
    private List<StoreMenuVo> filterMenuName(List<StoreMenuVo> menuVos) {

        if (CollectionUtils.isEmpty(menuVos)) {
            logger.error("filterMenuName fail due to menuVos is empty....");
            return menuVos;
        }

        return menuVos.stream().map(storeMenuVo -> {
            if (storeMenuVo.getMeta().getTitle().indexOf("店铺") > -1) {
                storeMenuVo.getMeta().setTitle(storeMenuVo.getMeta().getTitle().replace("店铺", "门店"));
            }
            return storeMenuVo;
        }).collect(Collectors.toList());
    }


    /**
     * 构造成父子结构的目录树
     *
     * @param menuVos 菜单
     * @return 返回构造好的菜单
     */
    private List<StoreMenuVo> buildChildren(List<StoreMenuVo> menuVos) {
        logger.debug("beging to buildChildren....");
        if (CollectionUtils.isEmpty(menuVos)) {
            logger.error("buildChildren fail due to menuVos is empty....");
            return menuVos;
        }

        // 过滤出一级菜单
        List<StoreMenuVo> firstMenus = menuVos.stream().filter(menuVo1 -> menuVo1.isGradeMatch(1)).collect(Collectors.toList());

        if (CollectionUtils.isEmpty(firstMenus)) {
            logger.error("this is no firstMenus....");
            return firstMenus;
        }

        // 设置一级菜单的二级菜单
        firstMenus.stream().forEach(firstMenu -> firstMenu.setChildren(menuVos.stream().
                filter(menuVo -> menuVo.isMatch(2, firstMenu.getAuthorityId())).collect(Collectors.toList())));

        // 设置二级菜单的三级菜单
        menuVos.stream().filter(menuVo1 -> menuVo1.isGradeMatch(2)).collect(Collectors.toList())
                .stream().forEach(secondMenu -> secondMenu.setChildren(menuVos.stream().
                filter(menuVo -> menuVo.isMatch(3, secondMenu.getAuthorityId())).collect(Collectors.toList())));

        return firstMenus;
    }
}
