package com.qiyee.shop.store.storemenu.service;

import com.qiyee.shop.entity.system.sotremenu.StoreMenu;
import com.qiyee.shop.entity.system.sotremenu.StoreMenuVo;

import java.util.List;

/**
 * @author administrator on 2017/6/7.
 */
public interface StoreMenuService {
    
    /**
     * 根据管理员id查询菜单
     *
     * @param customerId     会员id
     * @param isAdminAddShop 是否为admin端添加店铺  true 是  false 否
     * @return 管理员菜单实体类
     */
    List<StoreMenuVo> queryStoreMenu(Long customerId, boolean isAdminAddShop);

    /**
     * 根据管理员id查询菜单(后端角色管理使用)
     *
     * @param managerId 管理员id
     * @return 管理员菜单实体类 递归返回一级
     */
    List<StoreMenu> queryManagerMenus(Long managerId);

    /**
     * 查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）
     *
     * @param customerId 用户id
     * @return 返回所有的权限信息
     */
    List<String> queryCustomerPermissions(Long customerId);

}
