package com.qiyee.shop.store.storemenu.mapper;

import com.qiyee.shop.entity.system.sotremenu.StoreMenu;
import com.qiyee.shop.entity.system.sotremenu.StoreMenuVo;

import java.util.List;

/**
 * store端菜单mapper
 */
public interface StoreMenuMapper {

    /**
     * 根据管理员id查询菜单
     *
     * @param customerId 管理员id
     * @return 管理员菜单实体类
     */
    List<StoreMenu> queryStoreMenu(Long customerId);


    /**
     * 根据用户id查询菜单（前端路由使用 该查询查出来的都是类型为菜单类型的数据）
     *
     * @param customerId 用户id
     * @return 返回菜单
     */
    List<StoreMenuVo> queryMenuVo(Long customerId);


    /**
     * 查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）
     *
     * @param customerId 会员id
     * @return 返回用户的权限
     */
    List<String> queryCustomerPermissions(Long customerId);
}
