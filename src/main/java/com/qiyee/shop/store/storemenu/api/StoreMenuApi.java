package com.qiyee.shop.store.storemenu.api;

import com.qiyee.shop.entity.marketing.coupon.Coupon;
import com.qiyee.shop.entity.system.sotremenu.StoreMenu;
import com.qiyee.shop.entity.system.sotremenu.StoreMenuVo;
import com.qiyee.shop.store.storemenu.service.StoreMenuService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/store/storemenu")
@RestController
@Api("店铺用户/管理员接口")
public class StoreMenuApi {

    @Autowired
    private StoreMenuService storeMenuService;


    /**
     * 根据管理员id查询菜单
     *
     * @param customerId     会员id
     * @param isAdminAddShop 是否为admin端添加店铺  true 是  false 否
     * @return 管理员菜单实体类
     */
    @GetMapping("/queryStoreMenu/{isAdminAddShop}")
    @ApiOperation(value = "根据管理员id查询菜单", notes = "根据管理员id查询菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "Long", name = "customerId", value = "会员id"),
            @ApiImplicitParam(paramType = "path", dataType = "boolean", name = "isAdminAddShop", value = "是否为admin端添加店铺  true 是  false 否"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据管理员id查询菜单", response = Coupon.class)
    })
    public List<StoreMenuVo> queryStoreMenu(@RequestParam(value = "customerId") Long customerId,
                                            @PathVariable(value = "isAdminAddShop") boolean isAdminAddShop){
        return storeMenuService.queryStoreMenu(customerId,isAdminAddShop);
    }

    /**
     * 根据管理员id查询菜单(后端角色管理使用)
     *
     * @param managerId 管理员id
     * @return 管理员菜单实体类 递归返回一级
     */
    @GetMapping("/queryManagerMenus")
    @ApiOperation(value = "根据管理员id查询菜单(后端角色管理使用)", notes = "根据管理员id查询菜单(后端角色管理使用)")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "Long", name = "ManagerId", value = "管理员id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "根据管理员id查询菜单(后端角色管理使用)", response = Coupon.class)
    })
    public List<StoreMenu> queryManagerMenus(@RequestParam(value = "ManagerId") Long managerId){
        return storeMenuService.queryManagerMenus(managerId);
    }

    /**
     * 查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）
     *
     * @param customerId 用户id
     * @return 返回所有的权限信息
     */
    @GetMapping("/queryCustomerPermissions")
    @ApiOperation(value = "查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）", notes = "查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", dataType = "Long", name = "customerId", value = "用户id"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "查询店铺用户的权限 （后端鉴权使用 该查询这查出类型为接口的数据）", response = Coupon.class)
    })
    public List<String> queryCustomerPermissions(@RequestParam("customerId") Long customerId){
        return storeMenuService.queryCustomerPermissions(customerId);
    }
}
