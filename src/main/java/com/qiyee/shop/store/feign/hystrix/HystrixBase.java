package com.qiyee.shop.store.feign.hystrix;

import com.qiyee.shop.entity.model.Response;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

/**
 * @author admin
 */
@Slf4j
@Component
public class HystrixBase {


    public Response hystrixErrorInfo() {
        return hystrixErrorInfo(StringUtils.EMPTY);
    }

    public Response hystrixErrorInfo(String message) {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String errorInfo = "";
        errorInfo = this.getClass().getSimpleName() + "." + (stackTraceElements.length > 2 ? stackTraceElements[2].getMethodName() : "");
        log.error("HystrixError: {}, {}", errorInfo, this);
        StringBuilder messageBuilder = new StringBuilder("微服务异常:");
        if (StringUtils.isNotBlank(message)) {
            messageBuilder.append(message);
        }
        messageBuilder.append(" 调用服务方法 ").append(errorInfo);
        return Response.fail(1111,messageBuilder.toString());
    }
}
