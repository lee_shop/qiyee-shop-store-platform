package com.qiyee.shop.store.feign;


import com.qiyee.shop.api.spu.SpuDetail;
import com.qiyee.shop.dto.product.req.attention.HasAttentionReqDTO;
import com.qiyee.shop.dto.product.req.brand.QueryCustomBrandByStoreIdAndStatusReqDTO;
import com.qiyee.shop.dto.product.req.brand.QueryStoreBrandsReqDTO;
import com.qiyee.shop.dto.product.req.spu.*;
import com.qiyee.shop.dto.productread.req.CalculateFreightReqDTO;
import com.qiyee.shop.dto.productread.req.QueryCommissionSkuListReqDTO;
import com.qiyee.shop.dto.productread.req.QuerySpuDetailWithALLReqDTO;
import com.qiyee.shop.dto.productread.req.category.QueryAllParentSpuCategoryByIdReqDTO;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.sku.*;
import com.qiyee.shop.entity.product.spec.Spec;
import com.qiyee.shop.entity.product.spu.Spu;
import com.qiyee.shop.entity.product.spu.SpuAttributeValue;
import com.qiyee.shop.entity.product.spu.SpuCategory;
import com.qiyee.shop.entity.product.spu.SpuServiceSupport;
import com.qiyee.shop.store.feign.hystrix.ProductHystrix;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.ApiOperation;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;


@FeignClient(name = "qiyee-shop-product-base",contextId = "ProductFeign",fallback = ProductHystrix.class)
@Ignore
public interface ProductFeign {

    @GetMapping("/sku/queryFiveDataForAttentionStore")
    @ApiOperation(value = "根据店铺id查询前几条单品数据", notes = "根据店铺id查询前几条单品数据")
    Response<List<Sku>> queryFiveDataForAttentionStore(@RequestParam(value = "storeId") Long storeId);

    @GetMapping("/sku/querySkuCountByStoreId")
    @ApiOperation(value = "根据店铺id查询商品数量", notes = "根据店铺id查询商品数量")
    Response<Integer> querySkuCountByStoreId(@RequestParam(value = "storeId") long storeId);


    @PostMapping("/brand/queryStoreBrands")
    @ApiOperation(value = "根据店铺id查询主营品牌", notes = "根据店铺id查询主营品牌")
    Response<List<Brand>> queryStoreBrands(@RequestBody QueryStoreBrandsReqDTO queryStoreBrandsReqDTO);

    @PostMapping("/brand/queryCustomBrandByStoreIdAndStatus")
    @ApiOperation(value = "根据店铺id查询自定义品牌", notes = "根据店铺id查询自定义品牌")
    Response<List<Brand>> queryCustomBrandByStoreIdAndStatus(@RequestBody QueryStoreBrandsReqDTO queryStoreBrandsReqDTO);

    @GetMapping("/category/queryTwoCategoryByStoreId/{storeId}")
    @ApiOperation(value = "根据店铺id查询签约的二级分类", notes = "根据店铺id查询签约的二级分类")
    Response<List<Category>> queryTwoCategoryByStoreId(@PathVariable(value = "storeId") long storeId);

    @GetMapping("/category/queryThreeCategoryByStoreId/{storeId}")
    @ApiOperation(value = "根据店铺id查询签约三级分类", notes = "根据店铺id查询签约三级分类")
    Response<List<Category>> queryThreeCategoryByStoreId(@PathVariable(value = "storeId") long storeId);

    @GetMapping("/category/queryFirstCategoryByStoreId/{storeId}")
    @ApiOperation(value = "根据店铺id查询签约的一级分类", notes = "根据店铺id查询签约的一级分类")
    Response<List<Category>> queryFirstCategoryByStoreId(@PathVariable(value = "storeId") long storeId);
}
