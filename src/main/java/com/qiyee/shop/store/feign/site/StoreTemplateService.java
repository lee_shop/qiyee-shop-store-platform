package com.qiyee.shop.store.feign.site;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@FeignClient(name = "qiyee-shop-site", contextId = "StoreTemplateService")
@RequestMapping(value = "/store/template")
public interface StoreTemplateService {

    /**
     * 初始化模板
     *
     * @param storeId 店铺id
     * @return 1：成功
     */
    @PostMapping("/initTemplate/{storeId}")
    int initTemplate(@PathVariable("storeId") long storeId);
}
