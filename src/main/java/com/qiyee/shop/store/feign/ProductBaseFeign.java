package com.qiyee.shop.store.feign;

import com.qiyee.shop.dto.product.req.brand.*;
import com.qiyee.shop.dto.product.req.category.*;
import com.qiyee.shop.dto.product.req.spec.*;
import com.qiyee.shop.dto.product.req.spu.*;
import com.qiyee.shop.dto.product.req.type.AddTypeReqDTO;
import com.qiyee.shop.dto.product.req.type.BatchDeleteTypesReqDTO;
import com.qiyee.shop.dto.product.req.type.QueryTypesReqDTO;
import com.qiyee.shop.dto.product.res.BrandApplyResDTO;
import com.qiyee.shop.dto.product.res.BrandResDTO;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.brand.BrandApply;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.category.StoreSignedCategory;
import com.qiyee.shop.entity.product.mobilecate.MobileCate;
import com.qiyee.shop.entity.product.sku.Sku;
import com.qiyee.shop.entity.product.spec.Spec;
import com.qiyee.shop.entity.product.spu.Spu;
import com.qiyee.shop.entity.product.spu.SpuCategory;
import com.qiyee.shop.entity.product.spu.StoreSpu;
import com.qiyee.shop.entity.product.spuimport.SpuImport;
import com.qiyee.shop.entity.product.type.Type;
import com.qiyee.shop.store.feign.hystrix.ProductBaseHystrix;
import com.qiyee.shop.util.PageHelper;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "qiyee-shop-product-base",contextId = "ProductBaseFeign",fallback = ProductBaseHystrix.class)
@Ignore
public interface ProductBaseFeign {





    @GetMapping("/brand/batchDeleteCustomBrand/{storeId}")
    @ApiOperation(value = "删除自定义品牌", notes = "删除自定义品牌")
    Response batchDeleteCustomBrand(@PathVariable("storeId") long storeId);


    @GetMapping("/brand/passCustomBrandByStoreId/{storeId}")
    @ApiOperation(value = "根据店铺id查找其自定义品牌并通过品牌审核", notes = "根据店铺id查找其自定义品牌并通过品牌审核")
    Response<Integer> passCustomBrandByStoreId(@PathVariable("storeId") Long storeId);

    @PostMapping("/brand/batchAddCustomBrand")
    @ApiOperation(value = "添加自定义品牌", notes = "添加自定义品牌")
    Response<Integer> batchAddCustomBrand(@Validated @RequestBody List<Brand> list);
    ////////////品牌接口列表结束



    @PostMapping("/brandApply/addStoreBrand")
    @ApiOperation(value = "开店-添加店铺品牌", notes = "开店-添加店铺品牌")
    Response<Integer> addStoreBrand(@RequestBody List<BrandApply> list);




    @PostMapping("/brandApply/passBrandAuditByStoreId")
    @ApiOperation(value = "根据店铺id查询品牌并通过审核", notes = "根据店铺id查询品牌并通过审核")
    Response<Integer> passBrandAuditByStoreId(@RequestParam(value = "storeId") Long storeId);



    @GetMapping("/brandApply/deleteStoreBrand")
    @ApiOperation(value = "删除店铺品牌", notes = "删除店铺品牌")
    Response<Integer> deleteStoreBrand(@RequestParam(value = "storeId") long storeId);


    @GetMapping("/storeCategory/deleteSignedCategory")
    @ApiOperation(value = "删除店铺签约分类", notes = "删除店铺签约分类")
    Response<Integer> deleteSignedCategory(@RequestParam(value = "storeId") long storeId);


    @PostMapping("/storeCategory/addSignedCategory")
    @ApiOperation(value = "批量插入店铺签约分类", notes = "批量插入店铺签约分类")
    Response<Integer> addSignedCategory(@RequestBody List<StoreSignedCategory> list);

    @PostMapping("/spu/updateShelvesStatusByStoreIds")
    @ApiOperation(value = "根据店铺id修改商品上下架状态", notes = "根据店铺id修改商品上下架状态")
    Response<Integer> updateShelvesStatusByStoreIds(@RequestBody @Validated UpdateShelvesStatusByStoreIdsReqDTO reqDTO);

}
