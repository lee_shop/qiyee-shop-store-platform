package com.qiyee.shop.store.feign.client;

import com.alibaba.fastjson.JSON;
import com.qiyee.shop.dto.product.req.brand.AddBrandReqDTO;
import com.qiyee.shop.dto.product.req.category.QuerySpuCategoryReqDTO;
import com.qiyee.shop.dto.product.req.spu.*;
import com.qiyee.shop.dto.product.res.BrandResDTO;
import com.qiyee.shop.entity.exception.BizException;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.brand.BrandApply;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.category.StoreSignedCategory;
import com.qiyee.shop.entity.product.sku.Sku;
import com.qiyee.shop.entity.product.spu.Spu;
import com.qiyee.shop.entity.product.spu.SpuCategory;
import com.qiyee.shop.entity.product.spu.StoreSpu;
import com.qiyee.shop.store.feign.ProductBaseFeign;
import com.qiyee.shop.util.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * @description: ProductBase客户端
 * @author: zhangming
 * @create: 2020-06-11 20:18
 **/
@Slf4j
@Service
@ApiIgnore
public class ProductBaseClient {


    @Autowired
    private ProductBaseFeign feign;


    /**
     * 根据店铺id查询所有商品分类
     *
     * @return
     */
    public Integer deleteStoreBrand(long storeId) throws BizException {
        Response<Integer>  response = feign.deleteStoreBrand(storeId);
        if(!response.isSuccess()){
            log.error("--删除店铺品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id查询品牌并通过审核
     *
     * @return
     */
    public Integer passBrandAuditByStoreId(long storeId) throws BizException {
        Response<Integer>  response = feign.passBrandAuditByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询品牌并通过审核失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 删除自定义品牌
     *
     * @return
     */
    public Integer batchDeleteCustomBrand(long storeId) throws BizException {
        Response<Integer>  response = feign.batchDeleteCustomBrand(storeId);
        if(!response.isSuccess()){
            log.error("--删除自定义品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 删除店铺签约分类
     *
     * @return
     */
    public Integer deleteSignedCategory(long storeId) throws BizException {
        Response<Integer>  response = feign.deleteSignedCategory(storeId);
        if(!response.isSuccess()){
            log.error("--删除店铺签约分类失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 批量插入店铺签约分类
     *
     * @return
     */
    public Integer addSignedCategory(List<StoreSignedCategory> list) throws BizException {
        Response<Integer>  response = feign.addSignedCategory(list);
        if(!response.isSuccess()){
            log.error("--批量插入店铺签约分类失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 开店-添加店铺品牌
     *
     * @return
     */
    public Integer addStoreBrand(List<BrandApply> list) throws BizException {
        Response<Integer>  response = feign.addStoreBrand(list);
        if(!response.isSuccess()){
            log.error("--开店-添加店铺品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 开店-添加店铺品牌
     *
     * @return
     */
    public Integer passCustomBrandByStoreId( Long storeId) throws BizException {
        Response<Integer>  response = feign.passCustomBrandByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--开店-添加店铺品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 添加自定义品牌
     *
     * @return
     */
    public Integer batchAddCustomBrand( List<Brand> brandList ) throws BizException {
        Response<Integer>  response = feign.batchAddCustomBrand(brandList);
        if(!response.isSuccess()){
            log.error("--添加自定义品牌--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id修改商品上下架状态
     *
     * @return
     */
    public Integer updateShelvesStatusByStoreIds(UpdateShelvesStatusByStoreIdsReqDTO reqDTO ) throws BizException {
        Response<Integer>  response = feign.updateShelvesStatusByStoreIds(reqDTO);
        if(!response.isSuccess()){
            log.error("--根据店铺id修改商品上下架状态--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }



}
