package com.qiyee.shop.store.feign.customer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.qiyee.shop.entity.customer.customer.Customer;

/**
 * 会员
 */
@Component
@FeignClient(name = "qiyee-shop-customer-platform")
public interface CustomerService {

    /**
     * 根据会员id查询会员信息(没有密码信息)
     *
     * @param customerId 会员id
     * @return 返回会员信息
     */
    @GetMapping("/customer/queryCustomerWithNoPasswordById/{customerId}")
    Customer queryCustomerWithNoPasswordById(@PathVariable("customerId") long customerId);

    /**
     * 更新会员storeId和type
     *
     * @param customer 会员实体类
     * @return 添加返回码
     */
    @PostMapping("/customer/updateStoreIdAndType")
    int updateStoreIdAndType(@RequestBody Customer customer);

    /**
     * 根据用户名称查询用户信息
     *
     * @param userName 用户名称(可能是用户名 , 邮箱或者手机号码)
     * @return 返回用户信息
     */
    @GetMapping("/customer/queryCustomerByName")
    Customer queryCustomerByName(@RequestParam("userName") String userName);

    /**
     * 根据店铺id查询会员id
     *
     * @param storeId 店铺id
     * @return 会员信息
     */
    @GetMapping("/customer/queryCustomerIdByStoreId")
    Customer queryCustomerIdByStoreId(@RequestParam("storeId") long storeId);
}
