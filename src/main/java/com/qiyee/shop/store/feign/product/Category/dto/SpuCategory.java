package com.qiyee.shop.store.feign.product.Category.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.qiyee.shop.store.util.CustomLocalDateTimeDeserializer;
import com.qiyee.shop.store.util.CustomLocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 商品分类实体类
 * <p>
 * Created by QiyeeShop on 2017/6/15.
 */
@Data
@ApiModel(description = "商品分类实体类")
public class SpuCategory {

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private long id;

    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称")
    private String name;

    /**
     * 上级分类id 0 表示没有上级
     */
    @ApiModelProperty(value = "上级分类id 0 表示没有上级")
    private long parentId;

    /**
     * 1 一级分类 2 二级分类 3 三级分类
     */
    @ApiModelProperty(value = "1 一级分类 2 二级分类 3 三级分类")
    private int grade;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private int sort;

    /**
     * 店铺id
     */
    @ApiModelProperty(value = "店铺id")
    private long storeId;

    /**
     * 删除标记  0 未删除 1 删除
     */
    @ApiModelProperty(value = "删除标记  0 未删除 1 删除")
    private String delFlag;

    /**
     * 子分类
     */
    @ApiModelProperty(value = "子分类")
    private List<SpuCategory> childCateGory;

    /**
     * 创建时间
     */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime modifyTime;

    /**
     * 删除时间
     */
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonDeserialize(using = CustomLocalDateTimeDeserializer.class)
    @ApiModelProperty(value = "删除时间")
    private LocalDateTime delTime;

    /**
     * 是否显示下级
     */
    @ApiModelProperty(value = "是否显示下级")
    private boolean showChild;

    /**
     * 判断是否为一级分类
     *
     * @return 一级分类返回true, 否则返回false
     */
    @JsonIgnore
    public boolean isFirst() {
        return 1 == this.grade;
    }

}
