/**  
 * All rights Reserved, Designed By www.qiyee.com
 * @Title: EsFeign.java
 * @Package com.qiyee.shop.store.feign.es
 * @Description: 描述
 * @author: xiongzhengsheng
 * @date: 2020年7月7日 下午7:36:05
 * @version V1.0
 * @Copyright: www.qiyee.com
 */
package com.qiyee.shop.store.feign.es;

import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.qiyee.shop.entity.model.Response;
import org.springframework.web.bind.annotation.RequestParam;

/**   
 * @ClassName: EsFeign
 * @Description: TODO
 * @author: xiongzhengsheng
 * @date: 2020年7月7日 下午7:36:05
 * @Copyright: www.qiyee.com
 */
@Component
@FeignClient(name = "qiyee-shop-search", contextId = "EsFeign")
public interface EsFeign {

	/**
	 * 所以商品信息
	 *
	 * @param spuId 商品id
	 */
	@GetMapping("/search/indexSpu")
	@ApiOperation(value = "索引商品信息", notes = "索引商品信息")
	Response<Boolean> indexSpu(@RequestParam(value = "spuId") Long spuId);

	/**
	 * 把所有单品放入es中
	 */
	@GetMapping("/search/indexAllSpu")
	@ApiOperation(value = "生成索引", notes = "生成索引")
	Response<Boolean> indexAllSpu();
}
