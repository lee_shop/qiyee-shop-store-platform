package com.qiyee.shop.store.feign.hystrix;

import com.qiyee.shop.api.spu.SpuDetail;
import com.qiyee.shop.dto.product.req.attention.HasAttentionReqDTO;
import com.qiyee.shop.dto.product.req.brand.QueryCustomBrandByStoreIdAndStatusReqDTO;
import com.qiyee.shop.dto.product.req.brand.QueryStoreBrandsReqDTO;
import com.qiyee.shop.dto.product.req.spu.*;
import com.qiyee.shop.dto.productread.req.CalculateFreightReqDTO;
import com.qiyee.shop.dto.productread.req.QueryCommissionSkuListReqDTO;
import com.qiyee.shop.dto.productread.req.QuerySpuDetailWithALLReqDTO;
import com.qiyee.shop.dto.productread.req.category.QueryAllParentSpuCategoryByIdReqDTO;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.sku.*;
import com.qiyee.shop.entity.product.spec.Spec;
import com.qiyee.shop.entity.product.spu.Spu;
import com.qiyee.shop.entity.product.spu.SpuAttributeValue;
import com.qiyee.shop.entity.product.spu.SpuCategory;
import com.qiyee.shop.entity.product.spu.SpuServiceSupport;
import com.qiyee.shop.store.feign.ProductFeign;
import com.qiyee.shop.util.PageHelper;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @description: 商品服务熔断组件
 * @author: zhangming
 * @create: 2020-06-11 21:44
 **/
@Component
public class ProductHystrix extends HystrixBase implements ProductFeign {


    @Override
    public Response<List<Sku>> queryFiveDataForAttentionStore(Long storeId) {
        return null;
    }

    @Override
    public Response<Integer> querySkuCountByStoreId(long storeId) {
        return null;
    }

    @Override
    public Response<List<Brand>> queryStoreBrands(QueryStoreBrandsReqDTO queryStoreBrandsReqDTO) {
        return null;
    }

    @Override
    public Response<List<Brand>> queryCustomBrandByStoreIdAndStatus(QueryStoreBrandsReqDTO queryStoreBrandsReqDTO) {
        return null;
    }

    @Override
    public Response<List<Category>> queryTwoCategoryByStoreId(long storeId) {
        return null;
    }

    @Override
    public Response<List<Category>> queryThreeCategoryByStoreId(long storeId) {
        return null;
    }

    @Override
    public Response<List<Category>> queryFirstCategoryByStoreId(long storeId) {
        return null;
    }
}
