package com.qiyee.shop.store.feign.comment;

import com.qiyee.shop.entity.customer.comment.StoreScore;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(name = "qiyee-shop-comment",contextId = "StoreCommentClientService")
public interface StoreCommentClientService {

    @GetMapping("/storecomment/queryStoreScore")
    StoreScore queryStoreScore(@RequestParam(value = "storeId") Long storeId);
}
