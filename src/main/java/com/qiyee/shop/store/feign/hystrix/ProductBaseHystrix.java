package com.qiyee.shop.store.feign.hystrix;

import com.qiyee.shop.dto.product.req.brand.*;
import com.qiyee.shop.dto.product.req.category.*;
import com.qiyee.shop.dto.product.req.spec.*;
import com.qiyee.shop.dto.product.req.spu.*;
import com.qiyee.shop.dto.product.req.type.AddTypeReqDTO;
import com.qiyee.shop.dto.product.req.type.BatchDeleteTypesReqDTO;
import com.qiyee.shop.dto.product.req.type.QueryTypesReqDTO;
import com.qiyee.shop.dto.product.res.BrandApplyResDTO;
import com.qiyee.shop.dto.product.res.BrandResDTO;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.brand.BrandApply;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.category.StoreSignedCategory;
import com.qiyee.shop.entity.product.mobilecate.MobileCate;
import com.qiyee.shop.entity.product.sku.Sku;
import com.qiyee.shop.entity.product.spec.Spec;
import com.qiyee.shop.entity.product.spu.Spu;
import com.qiyee.shop.entity.product.spu.SpuCategory;
import com.qiyee.shop.entity.product.spu.StoreSpu;
import com.qiyee.shop.entity.product.spuimport.SpuImport;
import com.qiyee.shop.entity.product.type.Type;
import com.qiyee.shop.store.feign.ProductBaseFeign;
import com.qiyee.shop.util.PageHelper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @description: 商品服务熔断组件
 * @author: zhangming
 * @create: 2020-06-11 21:44
 **/
@Component
public class ProductBaseHystrix extends HystrixBase implements ProductBaseFeign {


    @Override
    public Response batchDeleteCustomBrand(long storeId) {
        return null;
    }

    @Override
    public Response<Integer> passCustomBrandByStoreId(Long storeId) {
        return null;
    }

    @Override
    public Response<Integer> batchAddCustomBrand(List<Brand> list) {
        return null;
    }

    @Override
    public Response<Integer> addStoreBrand(List<BrandApply> list) {
        return null;
    }

    @Override
    public Response<Integer> passBrandAuditByStoreId(Long storeId) {
        return null;
    }

    @Override
    public Response<Integer> deleteStoreBrand(long storeId) {
        return null;
    }

    @Override
    public Response<Integer> deleteSignedCategory(long storeId) {
        return null;
    }

    @Override
    public Response<Integer> addSignedCategory(List<StoreSignedCategory> list) {
        return null;
    }

    @Override
    public Response<Integer> updateShelvesStatusByStoreIds(UpdateShelvesStatusByStoreIdsReqDTO reqDTO) {
        return null;
    }
}
