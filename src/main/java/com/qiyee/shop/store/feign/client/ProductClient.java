package com.qiyee.shop.store.feign.client;

import com.alibaba.fastjson.JSON;
import com.qiyee.shop.dto.product.req.brand.QueryStoreBrandsReqDTO;
import com.qiyee.shop.entity.exception.BizException;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.entity.product.brand.Brand;
import com.qiyee.shop.entity.product.category.Category;
import com.qiyee.shop.entity.product.sku.Sku;
import com.qiyee.shop.store.feign.ProductFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;


/**
 * @description: ProductBase客户端
 * @author: zhangming
 * @create: 2020-06-11 20:18
 **/
@Slf4j
@Service
@ApiIgnore
public class ProductClient {


    @Autowired
    private ProductFeign feign;

    /**
     * 根据店铺id查询主营品牌
     *
     * @return
     */
    public List<Brand> queryStoreBrands(QueryStoreBrandsReqDTO queryStoreBrandsReqDTO) throws BizException {
        Response<List<Brand>> response = feign.queryStoreBrands(queryStoreBrandsReqDTO);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询主营品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id查询自定义品牌
     *
     * @return
     */
    public List<Brand> queryCustomBrandByStoreIdAndStatus(QueryStoreBrandsReqDTO queryStoreBrandsReqDTO) throws BizException {
        Response<List<Brand>> response = feign.queryCustomBrandByStoreIdAndStatus(queryStoreBrandsReqDTO);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询自定义品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }

    /**
     * 根据店铺id查询自定义品牌
     *
     * @return
     */
    public List<Category> queryFirstCategoryByStoreId( long storeId) throws BizException {
        Response<List<Category>> response = feign.queryFirstCategoryByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询签约的一级分类失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }

    /**
     * 根据店铺id查询自定义品牌
     *
     * @return
     */
    public List<Category> queryTwoCategoryByStoreId( long storeId) throws BizException {
        Response<List<Category>> response = feign.queryTwoCategoryByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询签约的二级分类失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id查询签约三级分类
     *
     * @return
     */
    public List<Category> queryThreeCategoryByStoreId( long storeId) throws BizException {
        Response<List<Category>> response = feign.queryThreeCategoryByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询签约三级分类失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id查询前几条单品数据
     *
     * @return
     */
    public List<Sku> queryFiveDataForAttentionStore( long storeId) throws BizException {
        Response<List<Sku>> response = feign.queryFiveDataForAttentionStore(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询前几条单品数据失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
    /**
     * 根据店铺id查询商品数量
     *
     * @return
     */
    public Integer querySkuCountByStoreId(long storeId) throws BizException {
        Response<Integer> response = feign.querySkuCountByStoreId(storeId);
        if(!response.isSuccess()){
            log.error("--根据店铺id查询商品数量失败--responseStoreInfo: {}" , JSON.toJSONString(response));
            throw new BizException(response.getCode(),response.getMsg());
        }
        return response.getData();
    }
}
