package com.qiyee.shop.store.feign.client;

import com.alibaba.fastjson.JSON;
import com.qiyee.shop.entity.exception.BizException;
import com.qiyee.shop.entity.model.Response;
import com.qiyee.shop.store.feign.es.EsFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @description:
 * @author: zhangming
 * @create: 2020-06-15 09:56
 **/
@Slf4j
@Service
@ApiIgnore
public class EsClient {

    @Autowired
    private EsFeign feign;


    /**
     * 新增品牌
     *
     * @return
     */
    public Boolean indexSpu(long spuId) {
        try {
            Response<Boolean> response = feign.indexSpu(spuId);
            if(!response.isSuccess()){
                log.error("--新增品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
                throw new BizException(response.getCode(),response.getMsg());
            }
            return response.getData();
        }catch (Exception e){
            log.error("",e);
            return false;
        }
    }

    /**
     * 新增品牌
     *
     * @return
     */
    public Boolean indexAllSpu() {
        try {
            Response<Boolean> response = feign.indexAllSpu();
            if(!response.isSuccess()){
                log.error("--新增品牌失败--responseStoreInfo: {}" , JSON.toJSONString(response));
                throw new BizException(response.getCode(),response.getMsg());
            }
            return response.getData();
        }catch (Exception e){
            log.error("",e);
            return false;
        }
    }

}
