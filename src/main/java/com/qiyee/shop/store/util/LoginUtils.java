package com.qiyee.shop.store.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * mobile 登录帮助工具类
 *
 * @author administrator on  2019/11/27.
 */
public class LoginUtils {

    private static final LoginUtils INSTANCE = new LoginUtils();

    private LoginUtils() {
    }

    public static LoginUtils getInstance() {
        return INSTANCE;
    }

    /**
     * 将用户信息放入session中
     *
     * @param customerId 用户id
     */
    public void putCustomerIdToSession(Long customerId) {
        getRequest().getSession().setAttribute(CommonConstant.MOBILE_LOGIN_SESSION_KEY, customerId);
    }

    /**
     * 将openId放入session中(微信内置浏览器用)
     *
     * @param openId 用户唯一标识
     */
    public void putOpenIdAndUnionIdToSession(String openId, String unionId) {
        getRequest().getSession().setAttribute(CommonConstant.MOBILE_OPENID_SESSION_KEY, openId);
        getRequest().getSession().setAttribute(CommonConstant.MOBILE_UNIONID_SESSION_KEY, unionId);
    }

    /**
     * 判断是否登录
     *
     * @return 登录返回true  未登录返回false
     */
    public boolean isLogin() {
        return CommonConstant.NO_LOGIN_CUSTOMERID != getCustomerIdFromSession();
    }

    /**
     * 从session获取用户
     *
     * @return 获取用户id
     */
    public long getCustomerIdFromSession() {
        return Objects.nonNull(getRequest().getSession().getAttribute(CommonConstant.MOBILE_LOGIN_SESSION_KEY)) ? (long) getRequest().getSession().getAttribute(CommonConstant.MOBILE_LOGIN_SESSION_KEY) : CommonConstant.NO_LOGIN_CUSTOMERID;
    }

    /**
     * 从session获取openId
     *
     * @return 获取openId
     */
    public String getOpenIdFromSession() {
        return Objects.nonNull(getRequest().getSession().getAttribute(CommonConstant.MOBILE_OPENID_SESSION_KEY)) ? (String) getRequest().getSession().getAttribute(CommonConstant.MOBILE_OPENID_SESSION_KEY) : "";
    }

    /**
     * 从session获取unionId
     *
     * @return 获取unionId
     */
    public String getUnionIdFromSession() {
        return Objects.nonNull(getRequest().getSession().getAttribute(CommonConstant.MOBILE_UNIONID_SESSION_KEY)) ? (String) getRequest().getSession().getAttribute(CommonConstant.MOBILE_UNIONID_SESSION_KEY) : "";
    }

    /**
     * 获得request对象
     *
     * @return 返回request对象
     */
    private HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
    }
}
