package com.qiyee.shop.store.util;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ResponseUtil<T> implements Serializable {

    private static final long serialVersionUID = 6686419048438176898L;

    private static final int SUCCESS_CODE = 0;

    /**
     * 状态码
     */
    private Integer error;

    /**
     * 描述
     */
    private String message;

    /**
     * 业务数据
     */
    private T body;

    private ResponseUtil() {

    }

    public ResponseUtil<T> error(int error) {
        this.error = error;
        return this;
    }

    public ResponseUtil<T> body(T body) {
        this.body = body;
        return this;
    }

    private ResponseUtil(int status, String message, T body) {
        this.error = status;
        this.message = message;
        this.body = body;
    }

    public static <T> ResponseUtil getInstance(T body) {
        return new ResponseUtil<T>(SUCCESS_CODE, null, body);
    }

    public static <T> ResponseUtil getErrorCode(Integer code) {
        return new ResponseUtil<T>(code, null, null);
    }

    public static <T> ResponseUtil getErrorCodeAndMsg(Integer code, String message) {
        return new ResponseUtil<T>(code, message, null);
    }

    public static ResponseUtil ok() {
        return new ResponseUtil().error(SUCCESS_CODE);
    }

    public Boolean hasError() {
        return this.error != SUCCESS_CODE;
    }

    public ResponseUtil message(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        if (null == this.body) {
            this.body((T) new Object());
        }
        return JSON.toJSONString(this);
    }
}