FROM openjdk:8u212-jre
MAINTAINER qiyee qiyee@qiyee-tech.com
ENV LANG C.UTF-8

COPY target/qiyee-shop-store-platform.jar /qiyee/qiyee-shop-store-platform.jar
ADD http://172.16.1.111/agentfile/agent.zip /qiyee/skywalking/
RUN cd /qiyee/skywalking && unzip agent.zip
RUN echo "Asia/shanghai" > /etc/timezone
RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime